package common;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

public class testConfig {
	
    public static final String URL = "https://loadtest.sasretail.com/en/";
    
    //FIX THIS PLACE TO BE THE RIGHT PATH
    public static final String Path_TestData = "C:\\Users\\pmartinez\\workspace\\SasAutomation\\src\\common/";

    //MAKE SURE THIS IS THE RIGHT EXCEL SHEET
    public static final String File_TestData = "Data.xlsx";
    
    public static void setDriver() {
    	
		String exePath = "C:\\Users\\pmartinez\\Desktop\\automation_files\\chromedriver.exe";
		System.setProperty("webdriver.chrome.driver", exePath);	
		
    }
    
    public static void instantiateDriver(WebDriver driver) {
    	
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(testConfig.URL);
		
    }

    public static void setAndroid(WebDriver driver) {
    	
    	
    }
    
    public static void setCapabilities(WebDriver driver, DesiredCapabilities capabilities) {

        // Set automationName desired capability.
        capabilities.setCapability("automationName", "uiautomator2");
        
        // Set android deviceName desired capability. Set your device name.
        capabilities.setCapability("deviceName", "samsung-sm_t817v-33001df65b61925f");

        // Set BROWSER_NAME desired capability. It's Android in our case here.
        capabilities.setCapability(CapabilityType.BROWSER_NAME, "Android");

        // Set android VERSION desired capability. Set your mobile device's OS version.
        capabilities.setCapability(CapabilityType.VERSION, "7.0");

        // Set android platformName desired capability. It's Android in our case here.
        capabilities.setCapability("platformName", "Android");

        // Set android appPackage desired capability. 
        capabilities.setCapability("appPackage", "com.sas.android.test");

        // Set android appActivity desired capability. 
        capabilities.setCapability("appActivity", "com.sas.android.view.activity.LoginActivity");

        // Set android autoAcceptAlerts desired capability
        capabilities.setCapability("autoAcceptAlerts", true);
        
        // Set android unexpectedAlertBehavior desired capability
        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, "accept");
        
        // Set android unexpectedHandle desired capability
        capabilities.setCapability(CapabilityType.UNHANDLED_PROMPT_BEHAVIOUR, "accept");
        
        capabilities.setCapability("autoGrantPermissions", "true");
    }
}
