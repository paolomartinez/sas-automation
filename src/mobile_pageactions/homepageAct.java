package mobile_pageactions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import mobile_pageobjects.homepageObj;

public class homepageAct {

	private static WebElement start = null;
	private static WebElement viewProj = null;
	private static WebElement addExpense = null;
	private static WebElement yes = null;
	private static WebElement no = null;
	
	public static void Execute(WebDriver driver) {
		
		if(verifyHomepage(driver) == true) {
			
			System.out.println("homepage verification SUCCESS");
			
		} else {
			
			System.out.println("homepage verification SUCCESS");
		}

	}
	
	public static boolean verifyHomepage(WebDriver driver) {
		
		start = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(homepageObj.button_Start(driver)));
		
		viewProj = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(homepageObj.button_AddExpenses(driver)));
		
		addExpense = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(homepageObj.button_AddExpenses(driver)));
		
		if(start != null && viewProj != null && addExpense != null) {
			return true;
		}
		
		return false;
	}
}
