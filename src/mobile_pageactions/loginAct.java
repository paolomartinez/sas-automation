package mobile_pageactions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import mobile_pageobjects.loginObj;


public class loginAct {

	private static WebElement userField = null;
	private static WebElement passwordField = null;
	private static WebElement loginButton = null;
	private static WebElement loginError = null;
	
	public static void Execute(WebDriver driver, String sUserName, String sPassword) throws Exception {	
		
		allowAppPermission(driver);
		//TODO maybe get rid of this
		//Thread.sleep(3000);
		if(verifyLoginPage(driver)) {
			fillUser(driver, sUserName);
			fillPassword(driver, sPassword);
			submitLogin(driver);
			System.out.println("login execution COMPLETED");
		} else {
			System.out.println("login execution FAILED");
		}
	}

    public static void allowAppPermission(WebDriver driver) {
    	
    	while (driver.findElements(By.xpath("//*[@class='android.widget.Button'][2]")).size()>0) {  
   		 	driver.findElement(By.xpath("//*[@class='android.widget.Button'][2]")).click();
   	 	}
   }
	
	public static void fillUser(WebDriver driver, String sUserName) {
		loginObj.textbox_UserName(driver).sendKeys(sUserName);
	}
	
	
	public static void fillPassword(WebDriver driver, String sPassword) {
		loginObj.textbox_Password(driver).sendKeys(sPassword);
	}
	
	
	public static void submitLogin(WebDriver driver) throws InterruptedException{
		Thread.sleep(1000);
		loginObj.button_LogIn(driver).click();
	}

	public static boolean verifyLoginPage(WebDriver driver) {		
				
		//verify three controls
		userField = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginObj.textbox_UserName(driver)));
		
		passwordField = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginObj.textbox_Password(driver)));
		
		loginButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginObj.button_LogIn(driver)));
		
		//Asserts
		Assert.assertTrue(userField != null && passwordField != null && loginButton != null);

		//Check all three controls exist
		if(userField != null && passwordField != null && loginButton != null) {
			return true;
		}

		return false;		
	}
}
