package mobile_pageactions;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import mobile_pageobjects.homepageObj;
import mobile_pageobjects.storeObj;

public class storeAct {

	private static WebElement noShow = null;
	private static WebElement time = null;
	private static WebElement backToSched = null;
	private static WebElement continueToSurvey = null;
	
	public static void Execute(WebDriver driver) throws InterruptedException {
		
		homepageObj.button_Start(driver).click();
		Thread.sleep(5000);
	
		System.out.println("about to click");
		//TODO insert a CLICK HERE
		driver.findElement(By.id("button1")).click();
		System.out.println("just clicked");       
       
		if(verifyStore(driver) == true) {
			
			System.out.println("store verification SUCCESS");
		
		} else {
			
			System.out.println("store verification FAILED");
			
		}
	}
	
	
	public static boolean verifyStore(WebDriver driver) {
		
		noShow = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(storeObj.toggle_NoShow(driver)));
		
		time = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(storeObj.button_Time(driver)));
		
		backToSched = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(storeObj.button_BackToSchedule(driver)));
		
		continueToSurvey = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(storeObj.button_ContinueToSurvey(driver)));
		
		if(noShow != null &&
			time != null &&
			backToSched != null &&
			continueToSurvey != null) {

			return true;
		}
		
		return false;
	}
}
