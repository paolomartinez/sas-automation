package mobile_pageactions;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import mobile_pageobjects.storeObj;
import mobile_pageobjects.surveyObj;

public class surveyAct {

	private static WebElement start = null;
	private static WebElement backToStore = null;
	private static WebElement continueToSummary = null;
	
	public static void Execute(WebDriver driver, String answer) throws InterruptedException {
		
		storeObj.button_ContinueToSurvey(driver).click();
		Thread.sleep(3000);
		if(verifySurvey(driver) == true) {
			
			//TODO add a check here later to account for the different types of surveys, add helper methods accordingly
			fillSurvey_YesNo(driver, answer);
			surveyObj.button_ContinueToSummary(driver).click();
			Thread.sleep(3000);
			System.out.println("survey verification SUCCESS");
			
		} else {
			
			System.out.println("survey verification FAILED");
		}
	}
	
	public static boolean verifySurvey(WebDriver driver) {
		
		start = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(surveyObj.button_Start(driver)));
		
		backToStore = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(surveyObj.button_BackToStore(driver)));
		
		continueToSummary = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(surveyObj.button_ContinueToSummary(driver)));
		
		if(start != null &&
			backToStore != null &&
			continueToSummary != null) {
			
			return true;
		}
		
		return false;
	}
	
	public static void fillSurvey_YesNo(WebDriver driver, String answer) throws InterruptedException {
		
		surveyObj.button_Start(driver).click();
		Thread.sleep(3000);
		if(answer.toLowerCase().equals("no")) {
			
			surveyObj.button_No(driver).click();;
			surveyObj.button_Proceed(driver).click();
			
		} else {
			
			surveyObj.button_Yes(driver).click();
			surveyObj.button_Proceed(driver).click();
			
		}
	
	}
}
