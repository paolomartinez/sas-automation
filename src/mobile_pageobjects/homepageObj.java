package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class homepageObj {

	private static WebElement element = null;
	
	public static WebElement button_Start(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_projectstart"));
		
		return element;
		 
	}
	
	public static WebElement button_ViewProject(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_projectview"));
		
		return element;
	
	}
	
	public static WebElement button_AddExpenses(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_add_expenses"));
		
		return element;
		
	}
	
}
