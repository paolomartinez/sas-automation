package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class loginObj {

	private static WebElement element = null;
	
	public static WebElement textbox_UserName(WebDriver driver) {
		
		element = driver.findElement(By.id("user_name_edit"));
		
		return element;
		
	}
	
	public static WebElement textbox_Password(WebDriver driver) {
		
		element = driver.findElement(By.id("password_edit"));
		
		return element;
		
	}
	
	public static WebElement button_LogIn(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_login"));
		
		return element;
	}
}
