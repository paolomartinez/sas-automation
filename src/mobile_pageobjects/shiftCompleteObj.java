package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class shiftCompleteObj {

	private static WebElement element;
	
	public static WebElement button_EndTime(WebDriver driver) {
		
		element = driver.findElement(By.id("text_end_time"));
		
		return element;
		
	}
	
	public static WebElement clock_Hours(WebDriver driver) {
		
		element = driver.findElement(By.id("hours"));
		
		return element;
		
	}
	
	public static WebElement clock_Minutes(WebDriver driver) {
		
		element = driver.findElement(By.id("minutes"));
		
		return element;
		
	}
	
	public static WebElement clock_OK(WebDriver driver) {
		
		element = driver.findElement(By.id("button1"));
		
		return element;
		
	}
	
	public static WebElement ratingBar(WebDriver driver) {
		
		element = driver.findElement(By.id("shift_rating"));
		
		return element;
		
	}
	
	public static WebElement button_Save(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_groupUpdate"));
				
		return element;
		
	}
	
	public static WebElement textbox_Feedback(WebDriver driver) {
		
		element = driver.findElement(By.id("txt_notes"));
		
		return element;
		
	}
	
	public static WebElement button_CompleteShift(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_submit"));
		
		return element;
		
	}
	
}
