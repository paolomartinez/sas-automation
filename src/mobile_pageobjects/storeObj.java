package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class storeObj {

	private static WebElement element = null;
	
	public static WebElement toggle_NoShow(WebDriver driver) {
		
		element = driver.findElement(By.id("toggle_no_show"));
		
		return element;
		
	}
	
	public static WebElement button_Time(WebDriver driver) {
		
		element = driver.findElement(By.id("time_text"));
		
		return element;
		
	}
	
	public static WebElement button_BackToSchedule(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_previous"));
		
		return element;
		
	}
	
	public static WebElement button_ContinueToSurvey(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_next"));
		
		return element;
		
	}
}
