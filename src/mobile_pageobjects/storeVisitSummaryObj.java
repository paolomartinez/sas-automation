package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class storeVisitSummaryObj {

	private static WebElement element;
	
	public static WebElement button_Excellent(WebDriver driver) {
		
		element = driver.findElement(By.id("txt_excellent"));
		
		return element;
		
	}
	
	public static WebElement button_Good(WebDriver driver) {
		
		element = driver.findElement(By.id("Good"));
		
		return element;
		
	}
	
	public static WebElement button_Fair(WebDriver driver) {
		
		element = driver.findElement(By.id("Fair"));
		
		return element;
		
	}
	
	public static WebElement button_Poor(WebDriver driver) {
		
		element = driver.findElement(By.id("Poor"));
		
		return element;
		
	}
	
	public static WebElement textbox_AdditionalComments(WebDriver driver) {
		
		element = driver.findElement(By.id("comments_edit"));
		
		return element;
		
	}
	
	public static WebElement checkbox_ReqCallback(WebDriver driver) {
		
		element = driver.findElement(By.id("req_callback_check"));
		
		return element;
		
	}
	
	public static WebElement checkbox_ReceiveSummary(WebDriver driver) {
		
		element = driver.findElement(By.id("visit_summary_check"));
		
		return element;
		
	}
	
	public static WebElement textbox_PhoneNumber(WebDriver driver) {
		
		element = driver.findElement(By.id("phone_number"));
		
		return element;
		
	}
	
	public static WebElement textbox_Email(WebDriver driver) {
		
		element = driver.findElement(By.id("email_id"));
		
		return element;
		
	}
	
	public static WebElement button_AddStoreManager(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_add_store_manager"));
		
		return element;
		
	}
	
	public static WebElement textbox_ManagerName(WebDriver driver) {
		
		element = driver.findElement(By.id("txt_manager_name"));
		
		return element;
	}
	
	public static WebElement textbox_ManagerEmail(WebDriver driver) {
		
		element = driver.findElement(By.id("txt_manager_email"));
		
		return element;
		
	}
	
	public static WebElement textbox_ManagerPhone(WebDriver driver) {
		
		element = driver.findElement(By.id("txt_manager_no"));
		
		return element;
		
	}
	
	public static WebElement button_SubmitManager(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_add_new"));
		
		return element;
		
	}
	
	public static WebElement checkbox_ApproveSignature(WebDriver driver) {
		
		element = driver.findElement(By.id("req_approved"));
		
		return element;
		
	}
	
	public static WebElement button_GoBack(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_previous"));
		
		return element;
		
	}

	public static WebElement button_SaveContinue(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_next"));
		
		return element;
		
	}
	
	
}
