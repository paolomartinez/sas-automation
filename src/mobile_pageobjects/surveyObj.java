package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class surveyObj {

	private static WebElement element;
	
	public static WebElement button_Start(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_start_survey"));
		
		return element;
		
	}
	
	public static WebElement button_BackToStore(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_previous"));
		
		return element;
		
	}
	
	public static WebElement button_ContinueToSummary(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_next"));
		
		return element;
		
	}
	
	public static WebElement textbox_SurveyAnswer(WebDriver driver) {
		
		element = driver.findElement(By.id("edit_text_survey"));
		
		return element;
		
	}
	
	public static WebElement button_Yes(WebDriver driver) {
		
		element = driver.findElement(By.id("yes"));
		
		return element;
		
	}
	
	public static WebElement button_No(WebDriver driver) {
		
		element = driver.findElement(By.id("no"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_proceed"));
		
		return element;
		
	}
	
	public static WebElement button_Back(WebDriver driver) {
		
		element = driver.findElement(By.id("btn_go_back"));
		
		return element;
		
	}
	
}
