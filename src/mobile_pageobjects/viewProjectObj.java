package mobile_pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class viewProjectObj {

	private static WebElement element = null;
	
	public static WebElement button_Yes(WebDriver driver) {
		
		element = driver.findElement(By.id("yes"));
		
		return element;
		
	}
	
	public static WebElement button_No(WebDriver driver) {
		
		element = driver.findElement(By.id("no"));
		
		return element;
		
	}
	
	public static WebElement textbox_StartTime(WebDriver driver) {
		
		element = driver.findElement(By.id("print_shift_start_time"));
		
		return element;
		
	}
	
	public static WebElement textbox_EndTime(WebDriver driver) {
		
		element = driver.findElement(By.id("print_shift_end_time"));
		
		return element;
		
	}
	
	public static WebElement textbox_WorkHours(WebDriver driver) {
		
		element = driver.findElement(By.id("print_shift_work_hour"));
		
		return element;
		
	}
	
	
}
