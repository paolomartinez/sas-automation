package mobile_tests;

import io.appium.java_client.android.AndroidDriver;
import mobile_pageactions.homepageAct;
import mobile_pageactions.loginAct;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import common.testConfig;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class SASRetailTest {
   // private AndroidDriver driver;
	AppiumDriver<MobileElement> driver;
    @Before
    public void setUp() throws Exception {
    	
        // Created object of DesiredCapabilities class.
        DesiredCapabilities capabilities = new DesiredCapabilities();
        
		// TODO Move all this to be handled in dataHandling
        testConfig.setCapabilities(driver, capabilities);
		
        // Created object of RemoteWebDriver will all set capabilities.
        // Set appium server address and port number in URL string.
        System.out.println("Instantiating Android driver...");
        driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        System.out.println("...Instantiation SUCCESS!");

    	System.out.println("Commencing simple SAS test...");
    	
    	// login
    	loginAct.Execute(driver, "253teardrop@gmail.com", "P@ssword1");
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        
        // verify home page
        homepageAct.Execute(driver);
 //   	homepageObj.button_Start(driver).click();
    	
    	
/*    	
        // set up appium
        File classpathRoot = new File(System.getProperty("user.dir"));
        //TODO: Set correct ClassPath
        //File appDir = new File(classpathRoot, "\SASRetail\app\build\outputs\apk");
        File app = new File(appDir, "app-debug.apk");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("deviceName","Android");
        capabilities.setCapability("platformVersion", "6.0");
        capabilities.setCapability("app", app.getAbsolutePath());
        capabilities.setCapability("appPackage", "com.sas.android.test");
        capabilities.setCapability("appActivity", "android.widget.FrameLayout");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
 */
    	
    }

    @After
    public void tearDown() throws Exception {
        driver.quit();
    }


    @Test
    public void test_SASRetailTest() throws InterruptedException{
   
    	System.out.println("about to tap the start button");
    	TouchAction ac = new TouchAction(driver);
		MobileElement ele = (MobileElement) driver.findElement(By.id("btn_projectstart"));
		ac.tap(ele).perform().release();
    	
    	//TODO try the driver.findElement, then the page object, then the coordinates idk why this isn't working

    	System.out.println("just tapped  it");
    	Thread.sleep(3000);
    	
    	System.out.println("about to tap the yes button");
	//	MobileElement ele2 = (MobileElement) driver.findElement(By.id("button1"));
		ac.tap(1400,870).perform().release();	
	//	ac.tap(ele2).perform().release();
    	System.out.println("just tapped  it");
    	Thread.sleep(3000);

  /*
       assertThat(driver.findElement(By.id("button1")).isDisplayed(), is(true));
       //TODO: Action to check element[ResourceID:android.R.id.button1] is completely displayed;
       assertThat(driver.findElement(By.id("button1")).isEnabled(), is(true));
       assertEquals(driver.findElement(By.id("button1")).getText().toString(),"YES");
       //TODO: Action to check element[ResourceID:android.R.id.button1] is clickable;
       driver.findElement(By.xpath("/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.widget.FrameLayout[2]/android.widget.RelativeLayout")).click();
*/
    }
}