package mobile_tests;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import mobile_pageactions.homepageAct;
import mobile_pageactions.loginAct;
import mobile_pageactions.storeAct;

import org.openqa.selenium.remote.RemoteWebDriver;

import common.testConfig;

public class homepageTest {
	
	private static WebDriver driver;
	
	public static void main(String[] args) throws Exception {
		
        // Created object of DesiredCapabilities class.
        DesiredCapabilities capabilities = new DesiredCapabilities();
        
		// TODO Move all this to be handled in dataHandling
        testConfig.setCapabilities(driver, capabilities);
		
        // Created object of RemoteWebDriver will all set capabilities.
        // Set appium server address and port number in URL string.
        System.out.println("Instantiating Android driver...");
        driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        System.out.println("...Instantiation SUCCESS!");

    	System.out.println("Commencing simple SAS test...");
    	
    	loginAct.Execute(driver, "253teardrop@gmail.com", "P@ssword1");
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        homepageAct.Execute(driver);
        
        System.out.println("homepage test SUCCESS");
        
        storeAct.Execute(driver);
        System.out.println("wow you got this far");
        
        driver.quit();
        
	}

}
