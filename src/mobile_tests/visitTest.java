package mobile_tests;

import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import common.testConfig;
import io.appium.java_client.android.AndroidDriver;
import mobile_pageactions.homepageAct;
import mobile_pageactions.loginAct;
import mobile_pageactions.storeAct;
import mobile_pageactions.surveyAct;

public class visitTest {

	private static WebDriver driver;
	
	public static void main(String[] args) throws Exception {
		
        // Created object of DesiredCapabilities class.
        DesiredCapabilities capabilities = new DesiredCapabilities();
        
		// TODO Move all this to be handled in dataHandling
        testConfig.setCapabilities(driver, capabilities);
		
        // Created object of RemoteWebDriver will all set capabilities.
        // Set appium server address and port number in URL string.
        System.out.println("Instantiating Android driver...");
        driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        System.out.println("...Instantiation SUCCESS!");

    	System.out.println("Commencing simple SAS test...");
    	
    	// login
    	loginAct.Execute(driver, "253teardrop@gmail.com", "P@ssword1");
        driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
        
        // verify home page
        homepageAct.Execute(driver);
        
        // begin visit, verify store
        storeAct.Execute(driver);
        
        // complete survey
        //TODO make this data driven
        surveyAct.Execute(driver, "no");
        
        driver.quit();
        
	}
}