package pageActions;

import org.openqa.selenium.support.ui.Select;
import java.io.IOException;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addCustomerStep1Obj;
import pageObjects.leftMenuObj;

public class addCustomerStep1Act {

	private static WebElement uploadButton = null;
	private static WebElement customerLogo = null;
	private static WebElement customerName = null;
	private static WebElement customerType = null;
	private static WebElement parentCompany = null;
	private static WebElement address = null;
	private static WebElement country = null;
	private static WebElement state = null;
	private static WebElement city = null;
	private static WebElement zip = null;
	private static WebElement primaryPhone = null;
	private static WebElement proceed = null;
	private static WebElement preBillToggle = null;
	private static WebElement creditLevelToggle = null;
	private static Select oSelect;
	
	public static void Execute(WebDriver driver, String customerName, String customerType, String parentCompany,
								String address, String country, String state, String city, String zip,
								String contactName, String primaryPhone, String email, String paymentTerms, 
								String creditAmt, String preBill, String title, String department, String goodUserName, String goodAddress, String goodEmail) throws Exception {
		
		leftMenuObj.button_Customers(driver).click();
		leftMenuObj.button_AddCustomer(driver).click();
		
		if(verifyStep1(driver) == true) {
			
			//Thread.sleep(2000);
			//uploadLogo(driver); 
			fillName(driver, customerName);
			selectType(driver, customerType);
			fillAddress(driver, address);
			selectCountry(driver, country);
			selectState(driver, state);
			fillCity(driver, city);
			fillZip(driver, zip);
			fillAccountContact(driver, contactName);
			fillPrimaryPhone(driver, primaryPhone);
			fillEmail(driver, email);
			selectPayment(driver, paymentTerms);
			
			if(!creditAmt.equals("")) {
				creditLevelToggle.click();
				fillCredit(driver, creditAmt);
			}
			if (!preBill.equals("")) {
				preBillToggle.click();
				fillPrebill(driver, preBill);
			}
			
			addPrimaryContact(driver, contactName, title, email, primaryPhone, department);
			proceed = (new WebDriverWait(driver, 10))
					.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.button_Proceed(driver)));
			Thread.sleep(2000);
			proceed.click();
			System.out.println("add new customer step 1 SUCCESS");
			
		} else {
			
			System.out.println("add new customer step 1 FAILED");
			
		}
	}
	
	public static void clickProceed(WebDriver driver) {
		
		addCustomerStep1Obj.button_Proceed(driver).click();
		
	}
	
	public static boolean verifyStep1(WebDriver driver) {
		
		uploadButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.button_UploadLogo(driver)));
		
		customerName = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.textbox_CustomerName(driver)));
		
		customerType = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.select_CustomerType(driver)));
		
		parentCompany = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.select_ParentCompany(driver)));
		
		address = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.textbox_Address(driver)));
		
		country = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.select_Country(driver)));
		
		state = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.select_State(driver)));
		
		zip = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.textbox_Zip(driver)));
		
		primaryPhone = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.textbox_PrimaryPhone(driver)));
		
		creditLevelToggle = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.switch_CreditLevel(driver)));
		
		preBillToggle = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.switch_PreBill(driver)));
				
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs MAKE A HELPER FOR THIS IN TEST CONFIG OR SOMETHING
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/customer/step1";
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
		
		if(errorCheck == false && 
				uploadButton != null && 
				customerName != null && 
				customerType != null && 
				parentCompany != null && 
				address != null && 
				country != null && 
				state != null &&
				zip != null) {
			
			return true;
		}
		
		return false;
	}
	
	public static void uploadLogo(WebDriver driver) throws InterruptedException, IOException {
		
		//uploadLogo.click();
		addCustomerStep1Obj.button_UploadLogo(driver).click();
		Runtime.getRuntime().exec("C:\\Users\\pmartinez\\Desktop\\automation_files\\upload.exe");
		Thread.sleep(2000);
		customerLogo = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep1Obj.picture_customerLogo(driver)));
		Assert.assertTrue(customerLogo != null);
	}
	
	public static void fillName(WebDriver driver, String sUserName) {
		
		addCustomerStep1Obj.textbox_CustomerName(driver).sendKeys(sUserName);
		
	}
	
	public static void clearName(WebDriver driver) {
		
		addCustomerStep1Obj.textbox_CustomerName(driver).clear();
	}
	
	public static void selectType(WebDriver driver, String type) {
		
		oSelect = new Select(addCustomerStep1Obj.select_CustomerType(driver));
		oSelect.selectByVisibleText(type);
		
	}
	
	public static void selectParentCompany(WebDriver driver, String company) {
		
		oSelect = new Select(addCustomerStep1Obj.select_ParentCompany(driver));
		oSelect.selectByVisibleText(company);
		
	}
	
	public static void fillAddress(WebDriver driver, String address) {
		
		addCustomerStep1Obj.textbox_Address(driver).sendKeys(address);
		
	}
	
	public static void clearAddress(WebDriver driver) {
		
		addCustomerStep1Obj.textbox_Address(driver).clear();
		
	}
	
	public static void selectCountry(WebDriver driver, String country) {
		
		oSelect = new Select(addCustomerStep1Obj.select_Country(driver));
		oSelect.selectByVisibleText(country);
		
	}
	
	public static void selectState(WebDriver driver, String state) {
		
		oSelect = new Select(addCustomerStep1Obj.select_State(driver));
		oSelect.selectByVisibleText(state);
		
	}
	
	public static void fillCity(WebDriver driver, String city) {
		
		addCustomerStep1Obj.textbox_City(driver).sendKeys(city);
		addCustomerStep1Obj.button_AutocompleteCity(driver).click();

	}
	
	
	public static void fillZip(WebDriver driver, String zip) {
		
		addCustomerStep1Obj.textbox_Zip(driver).sendKeys(String.valueOf(zip));
		
	}
	
	public static void fillAccountContact(WebDriver driver, String contact) {
		
		addCustomerStep1Obj.textbox_AccountContact(driver).sendKeys(contact);
		
	}
	
	public static void fillPrimaryPhone(WebDriver driver, String number) {
		
		addCustomerStep1Obj.textbox_PrimaryPhone(driver).sendKeys(String.valueOf(number));
		
	}
	
	public static void fillEmail(WebDriver driver, String email) {
		
		addCustomerStep1Obj.textbox_Email(driver).sendKeys(email);
		
	}
	
	public static void clearEmail(WebDriver driver) {
		
		addCustomerStep1Obj.textbox_Email(driver).clear();
		
	}
	
	public static void selectPayment(WebDriver driver, String terms) {
		
		oSelect = new Select(addCustomerStep1Obj.select_PaymentTerms(driver));
		oSelect.selectByVisibleText(terms);
		
	}
	
	public static void toggleCreditCheck(WebDriver driver) {
		
		addCustomerStep1Obj.switch_CreditLevel(driver).click();
		
	}
	
	public static void fillCredit(WebDriver driver, String price) {
		
		addCustomerStep1Obj.textbox_CreditAmount(driver).sendKeys(price);
		
	}
	
	public static void fillPrebill(WebDriver driver, String percentage) {
		
		addCustomerStep1Obj.textbox_PreBillAmt(driver).click();
		addCustomerStep1Obj.textbox_PreBillAmt(driver).sendKeys(Keys.BACK_SPACE);
		addCustomerStep1Obj.textbox_PreBillAmt(driver).sendKeys(Keys.BACK_SPACE);		
		addCustomerStep1Obj.textbox_PreBillAmt(driver).sendKeys(percentage);
		
	}
	
	public static void addBillingInfo(WebDriver driver, String name, String title, 
									  String email, String invoiceEmail, String phoneNumber ) {
		
		addCustomerStep1Obj.button_BillingInfo(driver).click();
		addCustomerStep1Obj.textbox_BillingName(driver).sendKeys(name);
		addCustomerStep1Obj.textbox_BillingInfoTitle(driver).sendKeys(title);
		addCustomerStep1Obj.textbox_Email(driver).sendKeys(email);
		addCustomerStep1Obj.textbox_InvoiceSpecificEmail(driver).sendKeys(invoiceEmail);
		addCustomerStep1Obj.textbox_PhoneNumber(driver).sendKeys(phoneNumber);
		addCustomerStep1Obj.button_AddBillingContact(driver).click();
		
	}
	
	public static void addPrimaryContact(WebDriver driver, String name, String title,
											String email, String phoneNumber, String department) throws InterruptedException {
		
		addCustomerStep1Obj.button_PrimaryContacts(driver).click();
		
		//create a wait so that it has time to load everything
		//this works for now
		Thread.sleep(2000);
		
		addCustomerStep1Obj.textbox_BillingName(driver).sendKeys(name);
		addCustomerStep1Obj.textbox_PrimaryContactTitle(driver).sendKeys(title);
		addCustomerStep1Obj.textbox_PrimaryContactEmail(driver).sendKeys(email);
		addCustomerStep1Obj.textbox_PhoneNumber(driver).sendKeys(phoneNumber);
		addCustomerStep1Obj.textbox_Department(driver).sendKeys(department);
		addCustomerStep1Obj.button_AddBillingContact(driver).click();
		
	}
	
	
}
