package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addCustomerStep1Obj;
import pageObjects.addCustomerStep2Obj;

public class addCustomerStep2Act {

	private static WebElement importStores = null;
	private static WebElement addStore = null;
	private static WebElement importCategories = null;
	private static WebElement addCategory = null;
	private static WebElement proceed = null;
	private static WebElement okButton = null;
	private static Select oSelect;
	
	//NOTE: There are no tests for the "Import" actions yet (what files am I supposed to use to test this?)
	public static void Execute(WebDriver driver, String storeName, String storeNum, String address,
								String country, String state, String city, String zip,
								String phone, String email, String categoryNum, String categoryName, String photoReq) throws InterruptedException {
		
		if(verifyStep2(driver) == true) {
			
			//Add New Store
			addStore.click();
			fillStore(driver, storeName, storeNum, address, country, state, city, zip, phone, email);
			addCustomerStep2Obj.button_AddNew(driver).click();
			
			//Add New Category
			//NOTE: this works for now, but it slows everything down. not sure how else to fix this bc the wait object doesn't work
			Thread.sleep(3000);

			addCategory.click();
			fillCategory(driver, categoryNum, categoryName, photoReq);
			
			proceed = (new WebDriverWait(driver, 30))
					.until(ExpectedConditions.elementToBeClickable(addCustomerStep2Obj.button_Proceed(driver)));
			Thread.sleep(1000);
			proceed.click();
			System.out.println("add new customer step 2 SUCCESS");
			
		} else {
			
			System.out.println("add new customer step 2 FAILED");
			
		}
	}
	
	public static void clickProceed(WebDriver driver) {
		
		addCustomerStep2Obj.button_Proceed(driver).click();
		
	}
	
	public static boolean verifyStep2(WebDriver driver) {
		
		importStores = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep2Obj.button_ImportStores(driver)));
		
		addStore = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep2Obj.button_AddStore(driver)));
		
		importCategories = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep2Obj.button_ImportCategories(driver)));
		
		addCategory = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep2Obj.button_AddCategory(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs MAKE A HELPER FOR THIS IN TEST CONFIG OR SOMETHING
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/customer/step2";
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
		
		if(importStores != null && addStore != null && importCategories != null && addCategory != null
				&& errorCheck == false) {
			return true;
		}
		
		return false;
	}
	
	public static void fillStore(WebDriver driver, String storeName, String storeNum, String storeAddress, String country,
									String state, String city, String zip, String phone, String email) throws InterruptedException {
		
		//wait for everything to load, this works for now
		Thread.sleep(1000);
		
		fillStoreName(driver, storeName);
		fillStoreNum(driver, storeNum);
		fillStoreAddress(driver, storeAddress);
		selectCountry(driver, country);
		selectState(driver, state);
		selectCity(driver, city);
		fillZip(driver, zip);
		fillPhone(driver, phone);
		fillEmail(driver, email);
		
	}
	
	public static void fillCategory(WebDriver driver, String categoryNum, String categoryName, String photoReq) throws InterruptedException {
		
		//wait for everything to load, this works for now
		Thread.sleep(2000);
		
		fillCategoryNum(driver, categoryNum);
		fillCategoryName(driver, categoryName);
		togglePhotoReq(driver, photoReq);
		addCustomerStep2Obj.toggle_CategoryPhotoReq(driver).sendKeys(Keys.RETURN);
		
	}
	
	public static void fillStoreName(WebDriver driver, String storeName) {
		
		addCustomerStep2Obj.textbox_StoreName(driver).sendKeys(storeName);
		
	}
	
	public static void fillStoreNum(WebDriver driver, String storeNum) {
		
		addCustomerStep2Obj.textbox_StoreNumber(driver).sendKeys(storeNum);
		
	}
	
	public static void fillStoreAddress(WebDriver driver, String address) {
		
		addCustomerStep2Obj.textbox_StoreAddress(driver).sendKeys(address);
		
	}
	
	public static void selectCountry(WebDriver driver, String country) {
		
		oSelect = new Select(addCustomerStep2Obj.select_StoreCountry(driver));
		oSelect.selectByVisibleText(country);
		
	}
	
	public static void selectState(WebDriver driver, String state) throws InterruptedException {
		
		/*addCustomerStep2Obj.select_StoreState(driver).sendKeys(state);
		Thread.sleep(1000);
		*/
		oSelect = new Select(addCustomerStep2Obj.select_StoreState(driver));
		oSelect.selectByVisibleText(state);
		
	}
	
	public static void selectCity(WebDriver driver, String city) {
		
		addCustomerStep2Obj.select_StoreCity(driver).sendKeys(city);
		addCustomerStep2Obj.button_AutocompleteCity(driver).click();
		
	}
	
	public static void fillZip(WebDriver driver, String zip) {
		
		addCustomerStep2Obj.textbox_StoreZip(driver).sendKeys(zip);
		
	}
	
	public static void fillPhone(WebDriver driver, String phone) {
		
		addCustomerStep2Obj.textbox_StorePhone(driver).sendKeys(phone);
		
	}
	
	public static void fillEmail(WebDriver driver, String email) {
		
		addCustomerStep2Obj.textbox_StoreEmail(driver).sendKeys(email);
		
	}
	
	public static void fillCategoryNum(WebDriver driver, String categoryNum) {
		
		addCustomerStep2Obj.textbox_CategoryNum(driver).sendKeys(categoryNum);
		
	}
	
	public static void fillCategoryName(WebDriver driver, String categoryName) {
		
		addCustomerStep2Obj.textbox_CategoryName(driver).sendKeys(categoryName);
		
	}
	
	public static void togglePhotoReq(WebDriver driver, String photoReq) {
		
		if(photoReq.equals("Yes")) {
		
			addCustomerStep2Obj.toggle_CategoryPhotoReq(driver).click();
			
		} 
	}
}
