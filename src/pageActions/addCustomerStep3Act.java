package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addCustomerStep1Obj;
import pageObjects.addCustomerStep3Obj;

public class addCustomerStep3Act {

	private static WebElement cancelButton = null;
	private static WebElement saveAsDraft = null;
	private static WebElement submitButton = null;
	private static WebElement documents = null;
	
	public static void Execute(WebDriver driver, String customerName, String customerType, String address, 
								String country, String state, String city, String zip, String contactName, 
								String primaryPhone, String email, String paymentTerms, String creditAmt, 
								String preBill, String title, String department, String storeName, 
								String storeNum, String storeAddress, String storeState, String storeCity, 
								String storeZip, String storePhone, String storeEmail, String categoryNum, 
								String categoryName, String photoReq, String goodUserName, String goodAddress, String goodEmail) throws InterruptedException {
		
		if(verifyStep3(driver, customerName, customerType,
				address, country, state, city, zip,
				contactName, primaryPhone, email, paymentTerms,
				creditAmt, preBill, title, department,
				storeName, storeNum, storeAddress,  
				storeState, storeCity, storeZip, storePhone, 
				storeEmail, categoryNum, categoryName, photoReq) == true) {
			
			submit(driver);

			boolean noError = driver.findElements(By.xpath("//H2[text()='Error!']")).size()==0;
			
			if(noError == false) {	
				
				fixStep1(driver, goodUserName, goodAddress, goodEmail, 
						 title, storePhone, department);
				
			}
			
			Thread.sleep(5000);
			System.out.println("add new customer step 3 SUCCESS");
			
		} else {
			
			System.out.println("add new customer step 3 FAILED");
			
		}
	}
	
	public static void fixStep1(WebDriver driver, String goodUserName, String goodAddress, String goodEmail, 
								String title, String storePhone, String department) throws InterruptedException {
		
		String url = testConfig.URL + "/sasretail/customer/step1";
		driver.navigate().to(url);
		//driver.navigate().to("https://test01.sasretail.com/en/sasretail/customer/step1");
		Thread.sleep(5000);
		//change name to acceptable length
		addCustomerStep1Act.clearName(driver);
		addCustomerStep1Act.fillName(driver, goodUserName);
		
		//change address to acceptable length
		addCustomerStep1Act.clearAddress(driver);
		addCustomerStep1Act.fillAddress(driver, goodAddress);
						
		//change email to acceptable name
		addCustomerStep1Act.clearEmail(driver);
		System.out.println("goodEmail: " + goodEmail);
		addCustomerStep1Act.fillEmail(driver, goodEmail);
		
		//delete contact, refill with goodEmail
		addCustomerStep1Obj.button_DeleteBillingInfo(driver).click();
		Thread.sleep(2000);
		addCustomerStep1Obj.button_ConfirmDelete(driver).click();
		Thread.sleep(2000);
		addCustomerStep1Act.addPrimaryContact(driver, "hi", title, "good@gmail.com", storePhone, department);
		Thread.sleep(2000);
		
		//Resubmit everything
		addCustomerStep1Act.clickProceed(driver);
		//Fix later to be better
		Thread.sleep(4000);
		addCustomerStep2Act.clickProceed(driver);
		submit(driver);
	}
	
	public static boolean verifyStep3(WebDriver driver, String customerName, String customerType, String address,
										String country, String state, String city, String zip, String contactName, 
										String primaryPhone, String email, String paymentTerms, String creditAmt, 
										String preBill, String title, String department, String storeName, 
										String storeNum, String storeAddress, String storeState, String storeCity, 
										String storeZip, String storePhone, String storeEmail, String categoryNum, 
										String categoryName, String photoReq) {
		
		cancelButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep3Obj.button_Cancel(driver)));
		
		saveAsDraft = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep3Obj.button_SaveAsDraft(driver)));
		
		submitButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep3Obj.button_Submit(driver)));
		
		documents = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep3Obj.button_Documents(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/customer/step3";
		
		//if preBill is "" verify it says 0%
	    if(preBill.equals("")) {
		    java.util.List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(),'" + "0%" + "')]"));
		    Assert.assertTrue("Text not found!", list.size() > 0);
	    } else {
	    	java.util.List<WebElement> list = driver.findElements(By.xpath("//*[contains(text(), preBill)]"));
	    	Assert.assertTrue("prebill 25% not found", list.size() > 0);
	    }
	    
		//verify info from previous steps is present on the summary page
		boolean textFound = false;  
		try {
			
		    driver.findElement(By.xpath("//*[contains(text(), customerName)]"));
		    driver.findElement(By.xpath("//*[contains(text(), customerType)]"));
		    driver.findElement(By.xpath("//*[contains(text(), address)]"));
		    driver.findElement(By.xpath("//*[contains(text(), country)]"));
		    driver.findElement(By.xpath("//*[contains(text(), state)]"));
		    driver.findElement(By.xpath("//*[contains(text(), city)]"));
		    driver.findElement(By.xpath("//*[contains(text(), zip)]"));
		    driver.findElement(By.xpath("//*[contains(text(), contactName)]"));
		    driver.findElement(By.xpath("//*[contains(text(), primaryPhone)]"));
		    driver.findElement(By.xpath("//*[contains(text(), email)]"));
		    driver.findElement(By.xpath("//*[contains(text(), title)]"));
		    driver.findElement(By.xpath("//*[contains(text(), department)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeName)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeNum)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeAddress)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeState)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeCity)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeZip)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storePhone)]"));
		    driver.findElement(By.xpath("//*[contains(text(), storeEmail)]"));
		    driver.findElement(By.xpath("//*[contains(text(), categoryNum)]"));
		    driver.findElement(By.xpath("//*[contains(text(), categoryName)]"));
		    driver.findElement(By.xpath("//*[contains(text(), photoReq)]"));
		    
		    System.out.println("find all elements: SUCCESS");
		    textFound = true;
		    
		} catch (Exception e) {
			
			System.out.println("find all elements FAILED");
		    textFound = false;
		    
		}
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));		
		Assert.assertTrue("Text not found!", textFound == true);
		
		if(cancelButton != null && saveAsDraft != null && submitButton != null &&
				documents != null && errorCheck == false && textFound == true) {
			return true;
		}
		
		return false;
	}
	
	public static void saveAsDraft(WebDriver driver) {
		
		addCustomerStep3Obj.button_SaveAsDraft(driver).click();
		
	}
	
	public static void submit(WebDriver driver) {
		
		addCustomerStep3Obj.button_Submit(driver).click();
		
	}
}
