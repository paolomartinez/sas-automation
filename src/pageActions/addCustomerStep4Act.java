package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addCustomerStep4Obj;

public class addCustomerStep4Act {

	private static WebElement customer = null;
	private static WebElement customerID = null;
	private static WebElement backButton = null;
	
	public static void Execute(WebDriver driver, String customerName) {
		
		if(verifyStep4(driver, customerName) == true) {
			
			System.out.println("add new customer step 4 SUCCESS");
			
		} else {
			
			System.out.println("add new customer step 4 FAILED");
			
		}
	}
	
	public static boolean verifyStep4(WebDriver driver, String customerName) {
		
		customer = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep4Obj.label_Customer(driver)));
		
		customerID = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep4Obj.label_CustomerID(driver)));
		
		backButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addCustomerStep4Obj.button_Back(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/customer/step4";
		
		//verify info from previous steps is present on the summary page
		boolean textFound = false;  
		try {
			
		    driver.findElement(By.xpath("//*[contains(text(), customerName)]"));
		    System.out.println("find customer name: SUCCESS");
		    textFound = true;
		    
		} catch (Exception e) {
			
			System.out.println("find customer name: FAILED");
		    textFound = false;
		    
		}
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.contains(expectedURL));	
		Assert.assertTrue("Text not found!", textFound == true);
		
		if(customer != null && customerID != null && backButton != null &&
				errorCheck == false && textFound == true) {
			
			return true;
					
		}
		
		return false;
	}
	
}
