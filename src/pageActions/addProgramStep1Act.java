package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addCustomerStep1Obj;
import pageObjects.addProgramStep1Obj;
import pageObjects.leftMenuObj;

public class addProgramStep1Act {

	private static WebElement customerName = null;
	private static WebElement programName = null;
	private static WebElement startDate = null;
	private static WebElement endDate = null;
	private static WebElement proceedButton = null;
	private static Select oSelect;
	
	public static void Execute(WebDriver driver, String customerName, String programName, 
								String startDate, String endDate, String teamBasedProgram,
								String numTeams, String teamSize) throws InterruptedException {
		
		leftMenuObj.button_Program(driver).click();
		leftMenuObj.button_AddProgram(driver).click();
		Thread.sleep(2000);
		
		verifyCustomerNameBox(driver, customerName);
		
		if(verifyStep1(driver) == true) {
			
			fillProgramName(driver, programName);
			selectStartDate(driver, startDate);
			selectEndDate(driver, endDate);
			Thread.sleep(5000);
			proceedButton = (new WebDriverWait(driver, 15))
					.until(ExpectedConditions.elementToBeClickable(addProgramStep1Obj.button_Proceed(driver)));
			
			if(teamBasedProgram.toLowerCase().equals("yes")) {
				
				setTeam(driver, numTeams, teamSize);
				
			}
			proceedButton.click();
			proceedButton.click();
			Thread.sleep(5000);
			System.out.println("add new program step 1 SUCCESS");
			
		} else {
			
			System.out.println("add new program step 1 FAILED");
			
		}
	}
	
	public static void verifyCustomerNameBox(WebDriver driver, String sName) {
		
		customerName = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep1Obj.select_CustomerName(driver)));
		
		//verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/program/step1/";
		
		Assert.assertTrue(customerName != null);
		Assert.assertTrue(currURL.equals(expectedURL));
		
		selectCustomerName(driver, sName);
		
	}
	
	public static boolean verifyStep1(WebDriver driver) {
		
		programName = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep1Obj.text_ProgramName(driver)));
		
		startDate = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep1Obj.select_StartDate(driver)));
		
		endDate = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep1Obj.select_EndDate(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertFalse(errorCheck);
		
		if(errorCheck == false && customerName != null) {
			
			return true;
		}

		return false;
	}
	
	public static void selectCustomerName(WebDriver driver, String name) {
		
		oSelect = new Select(addProgramStep1Obj.select_CustomerName(driver));
		oSelect.selectByVisibleText(name);
		
	}
	
	public static void fillProgramName(WebDriver driver, String name) {
		
		addProgramStep1Act.programName.sendKeys(name);
		
	}
	
	public static void selectStartDate(WebDriver driver, String start) {
		
		addProgramStep1Act.startDate.sendKeys(start);
		
	}
	
	public static void selectEndDate(WebDriver driver, String end) {
		
		addProgramStep1Act.endDate.sendKeys(end);
		
	}
	
	public static void setTeam(WebDriver driver, String numTeams, String avgSize) {
		
		addProgramStep1Obj.toggle_TeamBasedProgram(driver).click();
		addProgramStep1Obj.select_NumTeams(driver).sendKeys(numTeams);
		addProgramStep1Obj.select_AvgTeamSize(driver).sendKeys(avgSize);
		
	}
}
