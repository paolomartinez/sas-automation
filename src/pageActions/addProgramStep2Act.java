package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addProgramStep2Obj;

public class addProgramStep2Act {

	private static WebElement addNewTeam = null;
	private static WebElement storeSearch = null;
	private static WebElement storeZip = null;
	private static WebElement proceed = null;
	
	public static void Execute(WebDriver driver, String zip) throws InterruptedException {
		
		if(verifyStep2(driver) == true) {
			
			storeZip.sendKeys(zip);
			addProgramStep2Obj.button_Search(driver).click();
			Thread.sleep(3000);
			assignStores(driver);
			proceed.click();
			Thread.sleep(3000);
			System.out.println("add program step 2 SUCCESS");
			
		} else {
			
			System.out.println("add program step 2 FAILED");
			
		}
	}
	
	public static boolean verifyStep2(WebDriver driver) {
		
		addNewTeam = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep2Obj.button_AddNewTeam(driver)));
		
		storeSearch = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep2Obj.textbox_StoreSearch(driver)));
		
		storeZip = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep2Obj.textbox_StoreZip(driver)));
		
		proceed = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep2Obj.button_Proceed(driver)));
		
		// verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		// verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/program/step2/";
		
		// asserts
		Assert.assertTrue(errorCheck == false);
		Assert.assertTrue(currURL.contains(expectedURL));
		
		if(addNewTeam != null && storeSearch != null && storeZip != null && proceed != null) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static void assignStores(WebDriver driver) {
		
		addProgramStep2Obj.button_SelectAll(driver).click();
		addProgramStep2Obj.button_AssignToTeam(driver).click();
		
	}
}
