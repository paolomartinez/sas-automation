package pageActions;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import common.dataHandling;
import pageObjects.addProgramStep3Obj;

public class addProgramStep3Act {

	private static WebElement proceed = null;
	
	public static void Execute(WebDriver driver, String lead, String supervisor, String merchandiser) throws InterruptedException {
		
		if(verifyStep3(driver) == true) {
			
			fillRoles(driver, lead, supervisor, merchandiser);
			Thread.sleep(2000);
			proceed.click();
			
			System.out.println("add program step 3 SUCCESS");
			
		} else {
			
			System.out.println("add program step 3 FAILED");
			
		}
	}
	
	public static boolean verifyStep3(WebDriver driver) {
		
		proceed = (new WebDriverWait(driver, 15))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep3Obj.button_Proceed(driver)));
		
		// verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		// verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/program/step3/";
		
		// asserts
		Assert.assertTrue(errorCheck == false);
		Assert.assertTrue(currURL.contains(expectedURL));
		
		if(proceed != null) {
			
			return true;
			
		}
		
		return false;
	}
	
	//TODO make this data driven, no hard-coding, use parameter order to determine the role of the employee
	//in excel sheet, maybe label the column as like "Lead", "Merchandiser", etc.
	//first box is always lead, second is field supervisor, rest is merchandiser
	public static void fillRoles(WebDriver driver, String lead, String supervisor, String merchandiser) throws InterruptedException {
		
		List<WebElement> roleList = driver.findElements(By.xpath("(//SELECT[@data-ng-model='member.role'])"));
		List<WebElement> employeeList = driver.findElements(By.xpath("(//INPUT[@type='text'])"));
			
		//TODO this is a hard-coded solution for a team of three
		//use data handling to read the top row
		//if reading it is "lead" "field supervisor" or "merchandiser" then fill in the respective box with the name and role
		//use the role from data Handling
		//otherwise don't do anything
		//this gets rid of the hard coding problem
		
		
		//loop through that top row of the excel sheet
		//if the string reads one of the roles then you have to select that
		for(int i = 1; i < 5; i++) {
			int j = i+1;
			
			String num = Integer.toString(i);
			System.out.println("num is : " + num);
			String path = "(//SELECT[@data-ng-model='member.role'])[" + num + "]";
			String employeePath = "(//INPUT[@type='text'])[" + j +"]";

			if(i == 1) {

				addProgramStep3Obj.select_Role(driver, path).sendKeys("Lead");
				addProgramStep3Obj.textbox_Employee(driver, employeePath).sendKeys(lead);
				Thread.sleep(5000);
				addProgramStep3Obj.textbox_Employee(driver, employeePath).sendKeys(Keys.RETURN);

			} 

			else if(i == 2) {

				addProgramStep3Obj.select_Role(driver, path).sendKeys("Field Supervisor");
				addProgramStep3Obj.textbox_Employee(driver, employeePath).sendKeys(supervisor);
				Thread.sleep(5000);
				addProgramStep3Obj.textbox_Employee(driver, employeePath).sendKeys(Keys.RETURN);

			} else if(i == 3) {
				
				addProgramStep3Obj.select_Role(driver, path).sendKeys("Merchandiser");
				addProgramStep3Obj.textbox_Employee(driver, employeePath).sendKeys(merchandiser);
				Thread.sleep(5000);
				addProgramStep3Obj.textbox_Employee(driver, employeePath).sendKeys(Keys.RETURN);
				
			}	
		} 
	}
}


