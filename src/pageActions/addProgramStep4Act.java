package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addProgramStep4Obj;

public class addProgramStep4Act {

	private static WebElement cancelButton = null;
	private static WebElement submitButton = null;
	
	//execute
	public static void Execute(WebDriver driver) throws InterruptedException {
		
		if(verifyStep4(driver)==true) {
			
			submitButton.click();
			Thread.sleep(5000);
			if(verifyFinalStep(driver) == true) {
				
				System.out.println("add program step 4 SUCCESS");
				
			} else {
				
				System.out.println("add program step 4 FAILED");
				
			}

		} else {
			
			System.out.println("add program step 4 FAILED");
			
		}
	}
	
	//verify
	public static boolean verifyStep4(WebDriver driver) {
		
		cancelButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep4Obj.button_Cancel(driver)));
		
		submitButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProgramStep4Obj.button_Submit(driver)));
		
		//verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/program/step4/";
		boolean urlCheck = currURL.contains(expectedURL);
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(urlCheck);
		
		if(cancelButton != null && submitButton != null && errorCheck == false) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static boolean verifyFinalStep(WebDriver driver) {
		
		Assert.assertTrue(addProgramStep4Obj.label_Program(driver) != null);
		Assert.assertTrue(addProgramStep4Obj.label_ProgramID(driver) != null);
		Assert.assertTrue(addProgramStep4Obj.button_Back(driver) != null);
		
		if(addProgramStep4Obj.button_Back(driver) != null && 
				addProgramStep4Obj.label_Program(driver) != null &&
				addProgramStep4Obj.label_ProgramID(driver) != null) {
			
			return true;
			
		}
		
		return false;
		
	}
}