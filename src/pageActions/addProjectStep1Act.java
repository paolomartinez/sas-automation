package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addProjectStep1Obj;
import pageObjects.leftMenuObj;

public class addProjectStep1Act {
	
	private static Select oSelect;
	
	// General
	private static WebElement projectName = null;
	private static WebElement selectCustomer = null;
	private static WebElement selectProgram = null;
	private static WebElement addProgram = null;
	
	// Project Details
	private static WebElement projectType = null;
	private static WebElement startDate = null;
	private static WebElement endDate = null;
	
	// Other
	private static WebElement addMember = null;
	private static WebElement proceed = null;
	
	public static void Execute(WebDriver driver, String projectName, String customer, String program,
								String projectType, String startDate, String endDate, String projectManager,
								String billPrepLead) throws InterruptedException {
		
		leftMenuObj.button_Projects(driver).click();
		leftMenuObj.button_AddProject(driver).click();
		Thread.sleep(2000);
		
		if(verifyStep1(driver) == true) {
			
			fillProjectName(driver, projectName);
			selectCustomer(driver, customer);
			Thread.sleep(1000);
			selectProgram(driver, program);
			selectProject(driver, projectType);
			selectStartDate(driver, startDate);
			selectEndDate(driver, endDate);
			
			Thread.sleep(3000);
			addProjectStep1Obj.button_AddMember(driver).click();
			Thread.sleep(2000);
			addNewMember(driver, projectManager, "Project Manager");
			Thread.sleep(4000);
			
			addProjectStep1Obj.button_AddMember(driver).click();
			Thread.sleep(2000);
			addNewMember(driver, billPrepLead, "Bill Prep Lead");
			Thread.sleep(4000);
			
			proceed.click();
			Thread.sleep(2000);
			System.out.println("add new project step 1 SUCCESS");
			
		} else { 
			
			System.out.println("add new project step 1 FAILED");
			
		}
	}
	
	public static boolean verifyStep1(WebDriver driver) {
		
		projectName = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.textbox_ProjectName(driver)));
		
		selectCustomer = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.select_Customer(driver)));
		
		selectProgram = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.select_Program(driver)));
		
		addProgram = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.button_AddProgram(driver)));
		
		projectType = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.select_ProjectType(driver)));
		
		startDate = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.select_StartDate(driver)));
		
		endDate = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.select_EndDate(driver)));
		
		addMember = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.button_AddMember(driver)));
		
		proceed = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep1Obj.button_Proceed(driver)));
		
		//verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/project/general";
	
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertTrue(currURL.equals(expectedURL));
		Assert.assertFalse(errorCheck);
		
		if(errorCheck == false && projectName != null && selectCustomer != null && selectProgram != null &&
				addProgram != null && projectType != null && startDate != null && endDate != null &&
				addMember != null && proceed != null) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	public static void fillProjectName(WebDriver driver, String projectName) {
		
		addProjectStep1Obj.textbox_ProjectName(driver).sendKeys(projectName);
		
	}
	
	public static void selectCustomer(WebDriver driver, String customer) {
		
		oSelect = new Select(addProjectStep1Obj.select_Customer(driver));
		oSelect.selectByVisibleText(customer);
		
	}
	
	public static void selectProgram(WebDriver driver, String program) {
		
		oSelect = new Select(addProjectStep1Obj.select_Program(driver));
		oSelect.selectByVisibleText(program);
		
	}
	
	public static void selectProject(WebDriver driver, String project) {
		
		oSelect = new Select(addProjectStep1Obj.select_ProjectType(driver));
		oSelect.selectByVisibleText(project);
		
	}
	
	public static void selectStartDate(WebDriver driver, String start) {
		
		addProjectStep1Obj.select_StartDate(driver).sendKeys(start);
		
	}
	
	public static void selectEndDate(WebDriver driver, String end) {
		
		addProjectStep1Obj.select_EndDate(driver).sendKeys(end);
		
	}
	
	public static void addNewMember(WebDriver driver, String memberName, String role) throws InterruptedException {
		
		oSelect = new Select(addProjectStep1Obj.select_Role(driver));

		if(role.toLowerCase().equals("project manager")) {
			
			addProjectStep1Obj.textbox_MemberName(driver).sendKeys(memberName);
			oSelect.selectByVisibleText(role);
			Thread.sleep(1000);
			addProjectStep1Obj.textbox_MemberName(driver).sendKeys(Keys.RETURN);
			
		} else if(role.toLowerCase().equals("bill prep lead")) {
			
			addProjectStep1Obj.textbox_MemberName(driver).sendKeys(memberName);
			oSelect.selectByVisibleText(role);
			Thread.sleep(1000);
			addProjectStep1Obj.textbox_MemberName(driver).sendKeys(Keys.RETURN);
			
		}
		
		addProjectStep1Obj.button_Save(driver).click();
		
	}
}
