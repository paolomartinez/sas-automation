package pageActions;

import java.util.Scanner;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addProjectStep2Obj;


public class addProjectStep2Act {

	private static Select oSelect;
	
	// Services
	private static WebElement serviceTypeBox = null;
	
	// Schedule
	private static WebElement occurrence = null;
	private static WebElement hoursPerVisit = null;
	
	// Stores
	private static WebElement addLocations = null;
	private static WebElement proceed = null;
	
	private static WebElement firstStore = null;
	
	public static void Execute(WebDriver driver, String serviceType, String occurrence, String days) throws InterruptedException {
		
		String services = getServices(driver, serviceType);
		System.out.println("services are: " + services);
	    			
		if(verifyStep2(driver) == true) {
			
			//TODO temporary hard-coded solution
		    if(services.contains("survey") && services.contains("distributionvoid") &&
		    		services.contains("nici") && services.contains("planogramcompliance") && 
		    		services.contains("categoryreset")) {
		    	
		    	fillServiceType(driver, "survey");
		    	fillServiceType(driver, "distribution void");
		    	fillServiceType(driver, "nici");
		    	fillServiceType(driver, "planogram compliance");
		    	fillServiceType(driver, "category reset");
		    	System.out.println("okay this part works");
		    	
		    } else if(services.contains("survey")) {
		    	
		    	fillServiceType(driver, "survey");
		    	
		    } else if(services.contains("distributionvoid")) {
		    	
		    	fillServiceType(driver, "distribution void");
		    	
		    } else if(services.contains("nici")) {
		    	
		    	fillServiceType(driver, "nici");
		    	
		    } else if(services.contains("planogramcompliance")) {
		    	
		    	fillServiceType(driver, "planogram compliance");
		    	
		    } else if(services.contains("categoryreset")) {
		    	
		    	fillServiceType(driver, "category reset");
		    	
		    }

			selectRecurrence(driver, occurrence);
			selectActiveDays(driver, days);
			addProjectStep2Obj.button_AddLocations(driver).click();
			//wait for 1.5 minutes because idk what else to do to help this load rn
			firstStore = (new WebDriverWait(driver, 60))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//LABEL[@class='customCB'])[1]")));
			
			//TODO modify this to be more customizable, right now it just selects the first store
			addLocations(driver);
			Thread.sleep(4000);
			
			addProjectStep2Obj.button_Proceed(driver).click();
			Thread.sleep(3000);
			
			System.out.println("add new project step 2 SUCCESS");
		
		} else {
			
			System.out.println("add new project step 2 FAILED");
			
		}
	}
	
	public static boolean verifyStep2(WebDriver driver) {
		
		serviceTypeBox = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep2Obj.textbox_ServiceType(driver)));
		
		occurrence = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep2Obj.select_Occurrence(driver)));
	
		hoursPerVisit = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep2Obj.textbox_HoursPerVisit(driver)));
	
		addLocations = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep2Obj.button_AddLocations(driver)));
		
		proceed = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep2Obj.button_Proceed(driver)));
		
		//verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/project/services";
	
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertTrue(currURL.equals(expectedURL));
		Assert.assertFalse(errorCheck);
		
		if(serviceTypeBox != null && occurrence != null && hoursPerVisit != null
				&& addLocations != null && proceed != null) {
			
			return true;
			
		}
			
		return false;
	}
	
	public static void fillServiceType(WebDriver driver, String type) {
		
		addProjectStep2Obj.textbox_ServiceType(driver).click();
		addProjectStep2Obj.textbox_ServiceType(driver).sendKeys(type);
		addProjectStep2Obj.textbox_ServiceType(driver).sendKeys(Keys.RETURN);
	
	}
	
	public static void selectRecurrence(WebDriver driver, String recurrence) {
		
		oSelect = new Select(addProjectStep2Obj.select_Occurrence(driver));
		oSelect.selectByVisibleText(recurrence);
		
	}
	
	public static void fillHoursPerVisit(WebDriver driver, String hours) {
		
		addProjectStep2Obj.textbox_HoursPerVisit(driver).sendKeys(hours);
		
	}
	
	public static void selectActiveDays(WebDriver driver, String days) {
		
		days = days.toLowerCase();
		if(days.contains("su")) {
			addProjectStep2Obj.button_Sunday(driver).click();
		}
		
		if(days.contains("mo")) {
			addProjectStep2Obj.button_Monday(driver).click();
		}
		
		if(days.contains("tu")) {
			addProjectStep2Obj.button_Tuesday(driver).click();
		}
		
		if(days.contains("we")) {
			addProjectStep2Obj.button_Wednesday(driver).click();
		}
		
		if(days.contains("th")) {
			addProjectStep2Obj.button_Thursday(driver).click();
		}
		
		if(days.contains("fr")) {
			addProjectStep2Obj.button_Friday(driver).click();
		}
		
		if(days.contains("sa")) {
			addProjectStep2Obj.button_Saturday(driver).click();
		}
	}
	
	public static void addLocations(WebDriver driver) {
		
		addProjectStep2Obj.button_firstStore(driver).click();
		addProjectStep2Obj.button_Submit(driver).click();
		
	}
	
	public static String getServices(WebDriver driver, String serviceType) {
		
	    System.out.println("Source: " + serviceType);
	    String oneWord = "";
	    for (String tag : serviceType.split("[\\s,;]+")) {
	    	oneWord += tag;
	    	System.out.println("Received tag: [" + tag + "]");
	    }
		
	    return oneWord;
	}
}
