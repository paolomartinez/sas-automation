package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addProjectStep3Obj;

public class addProjectStep3Act {

	private static Select oSelect;
	
	private static WebElement clientSearch = null;

	public static void Execute(WebDriver driver, String client, String billingContact, String loaContact, 
								String rateType, String priceType, String billRate, String ratePerVisit, String billingTerms) throws InterruptedException {
		
		
		/*String url = testConfig.URL + "/sasretail/project/billing";
		driver.navigate().to(url);
		Thread.sleep(2500);
		*/
		if(verifyStep3(driver) == true) {
			
			addBillingClient(driver, client, billingContact, loaContact, 
								rateType, priceType, billRate, ratePerVisit, billingTerms);
			
			addProjectStep3Obj.button_Proceed(driver).click();
			Thread.sleep(4000);
			System.out.println("add new project step 3 SUCCESS");
		
		} else {
			
			System.out.println("add new project step 3 FAILED");
			
		}
	}
	
	public static boolean verifyStep3(WebDriver driver) {
		
		clientSearch = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep3Obj.select_Client(driver)));
		
		//verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/project/billing";
	
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertTrue(currURL.equals(expectedURL));
		Assert.assertFalse(errorCheck);
		
		if(clientSearch != null) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static void selectClient(WebDriver driver, String client) throws InterruptedException {
		
		addProjectStep3Obj.select_Client(driver).click();
		addProjectStep3Obj.select_Client(driver).clear();
		addProjectStep3Obj.select_Client(driver).sendKeys(client);
		Thread.sleep(2000);
		addProjectStep3Obj.button_autocompleteClient(driver).click();
		
	}
	
	public static void selectBillingContact(WebDriver driver, String contact) {
		
		oSelect = new Select(addProjectStep3Obj.select_BillingContact(driver));
		oSelect.selectByVisibleText(contact);
		
	}
	
	public static void selectLOAContact(WebDriver driver, String contact) {
		
		oSelect = new Select(addProjectStep3Obj.select_LOAContact(driver));
		oSelect.selectByVisibleText(contact);
		
	}
	
	public static void selectBillingTerms(WebDriver driver, String terms) {
		
		oSelect = new Select(addProjectStep3Obj.select_BillingTerms(driver));
		oSelect.selectByVisibleText(terms);
	}
	
	public static void selectRateType(WebDriver driver, String type) {
		
		oSelect = new Select(addProjectStep3Obj.select_RateType(driver));
		oSelect.selectByVisibleText(type);
		
	}
	
	public static void selectPricingType(WebDriver driver, String type) {
		
		oSelect = new Select(addProjectStep3Obj.select_PricingType(driver));
		oSelect.selectByVisibleText(type);
		
	}
	
	public static void fillBillingRates(WebDriver driver, String rate, String ratePerVisit) {
		
		addProjectStep3Obj.textbox_BillingRate(driver).sendKeys(rate);
		addProjectStep3Obj.textbox_HourlyRate(driver).sendKeys(ratePerVisit);
		
	}
	
	public static void addBillingClient(WebDriver driver, String client, String billingContact, String loaContact,
										String rateType, String priceType, String billRate, 
										String ratePerVisit, String billingTerms) throws InterruptedException {
		
		selectClient(driver, client);
		selectBillingContact(driver, billingContact);
		selectLOAContact(driver, loaContact);
		addProjectStep3Obj.button_Add(driver).click();
		Thread.sleep(2000);
		selectRateType(driver, rateType);
		selectPricingType(driver, priceType);
		fillBillingRates(driver, billRate, ratePerVisit);
		selectBillingTerms(driver, billingTerms);
		addProjectStep3Obj.button_AddRates(driver).click();
		
	}
}
