package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addProjectStep4Obj;

public class addProjectStep4Act {

	private static WebElement cancel = null;
	private static WebElement save = null;
	private static WebElement submit = null;
	private static WebElement projectID = null;
	
	public static void Execute(WebDriver driver) {
		
		if(verifyStep4(driver) == true) {
			
			submit.click();
			projectID =(new WebDriverWait(driver, 10))
			.until(ExpectedConditions.elementToBeClickable(addProjectStep4Obj.label_ProjectID(driver)));
			
			Assert.assertTrue(projectID != null);
			System.out.println("add new project step 4 SUCCESS");
			
		} else {
			
			System.out.println("add new project step 4 FAILEd");
			
		}
	}
	
	public static boolean verifyStep4(WebDriver driver) {
		
		cancel = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep4Obj.button_Cancel(driver)));
		
		save = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep4Obj.button_Save(driver)));
		
		submit = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(addProjectStep4Obj.button_Submit(driver)));
		
		//verify URLs 
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/project/summary";
	
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertTrue(currURL.equals(expectedURL));
		Assert.assertFalse(errorCheck);
		
		if(cancel != null && save != null && submit != null) {
			
			return true;
			
		}
		
		return false;
	}
	
}
