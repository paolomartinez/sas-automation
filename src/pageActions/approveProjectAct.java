package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.approveProjectObj;
import pageObjects.manageProjectsObj;

public class approveProjectAct {

	private static WebElement approve = null;
	private static WebElement decline = null;

	public static void Execute(WebDriver driver) throws InterruptedException {
		
		String url = testConfig.URL + "sasretail/project/management/draft";
		driver.navigate().to(url);
		//driver.navigate().to("https://test01.sasretail.com/en/sasretail/project/management/draft");
		manageProjectsObj.sort_UnsortedStartDate(driver).click();
		Thread.sleep(5000);
		manageProjectsObj.sort_AscendingStartDate(driver).click();
		Thread.sleep(5000);
		manageProjectsObj.button_ManageMostRecentProj(driver).click();
		manageProjectsObj.button_ApproveReject(driver).click();
		Thread.sleep(3000);
		
		if(verify(driver) == true) {
			
			approve.click();
			Thread.sleep(2000);
			approveProjectObj.button_Submit(driver).click();
			Thread.sleep(3000);
			System.out.println("fin approval SUCCESS");
		
		} else {
			
			System.out.println("fin approval FAILED");
			
		}
	}
	
	public static void secondApproval(WebDriver driver) throws InterruptedException {
		
		String url = testConfig.URL + "sasretail/project/management/draft";
		driver.navigate().to(url);
		//driver.navigate().to("https://test01.sasretail.com/en/sasretail/project/management/draft");
		manageProjectsObj.sort_UnsortedStartDate(driver).click();
		Thread.sleep(5000);
		manageProjectsObj.sort_AscendingStartDate(driver).click();
		Thread.sleep(5000);
		
		manageProjectsObj.button_ManageMostRecentProj(driver).click();
		
		manageProjectsObj.button_ApproveReject(driver).click();
		Thread.sleep(3000);
		
		if(verify(driver) == true) {
			
			approve.click();
			Thread.sleep(2000);
			approveProjectObj.button_Submit(driver).click();
			Thread.sleep(3000);
			System.out.println("ops approval SUCCESS");
		
		} else {
			
			System.out.println("ops approval FAILED");
			
		}
	}
	
	public static boolean verify(WebDriver driver) {
		
		approve = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(approveProjectObj.button_Approve(driver)));
		
		decline = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(approveProjectObj.button_Decline(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertFalse(errorCheck);
		
		if(approve != null && decline != null) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static boolean verifyActiveStatus(WebDriver driver, String name) throws InterruptedException {
		
		manageProjectsObj.button_ActiveProjects(driver).click();
		Thread.sleep(3000);
		manageProjectsObj.button_Status(driver).click();
		Thread.sleep(4000);
		manageProjectsObj.button_DescendingStatus(driver).click();
		
		String projects = driver.findElement(By.tagName("body")).getText();
		if(projects.contains(name)) {
			
			System.out.println("active project verification SUCCESS");
			return true;
		
		}
		
		System.out.println("active project verification FAILED");
		return false;
	}
}
