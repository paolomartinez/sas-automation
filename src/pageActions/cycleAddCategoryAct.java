package pageActions;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.cycleAddCategoryObj;
import pageObjects.cycleAddNICIObj;
import pageObjects.cycleMgtObj;

public class cycleAddCategoryAct {

	private static WebElement planogramID = null;
	private static WebElement size = null;
	private static WebElement hours = null;
	private static WebElement alert = null;
	private static Select oSelect;
	
	public static void Execute(WebDriver driver, String category, String id, String size,
								String sizeType, String hours, String minutes, String url) throws InterruptedException {
		
		cycleMgtObj.button_CategoryReset(driver).click();
		Thread.sleep(2000);
		cycleAddCategoryObj.button_AddNewCategory(driver).click();
		if(verify(driver) == true) {
			
			selectCategory(driver, category);
			fillPlanogramID(driver, id);
			fillSize(driver, size, sizeType);
			fillHours(driver, hours, minutes);
			Thread.sleep(3000);
			cycleAddCategoryObj.button_Proceed(driver).click();;
			Thread.sleep(5000);
			selectStore(driver);
			cycleAddCategoryObj.button_Submit(driver).click();
			dismissAlert(driver);
			driver.navigate().to(url);
			Thread.sleep(3000);
			System.out.println("add category reset SUCCESS");
			
		} else {
			
			System.out.println("add category reset FAILED");
			
		}
	}
	
	public static boolean verify(WebDriver driver) {
		
		planogramID = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddCategoryObj.textbox_PlanogramID(driver)));
		
		size = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddCategoryObj.textbox_Size(driver)));
		
		hours = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddCategoryObj.textbox_Hours(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertFalse(errorCheck);
		
		if(planogramID != null && size != null && hours != null) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	public static void selectCategory(WebDriver driver, String category) {
		
		oSelect = new Select(cycleAddCategoryObj.select_Category(driver));
		oSelect.selectByVisibleText(category);

	}
	
	public static void fillPlanogramID(WebDriver driver, String id) {
		
		cycleAddCategoryObj.textbox_PlanogramID(driver).sendKeys(id);
		
	}
	
	public static void fillSize(WebDriver driver, String size, String sizeType) {
		
		cycleAddCategoryObj.textbox_Size(driver).sendKeys(size);
		cycleAddCategoryObj.select_SizeType(driver).sendKeys(sizeType);
		
	}
	
	public static void fillHours(WebDriver driver, String hours, String minutes) {
		
		cycleAddCategoryObj.textbox_Hours(driver).sendKeys(hours);
		cycleAddCategoryObj.textbox_Minutes(driver).sendKeys(minutes);
		
	}
	
	public static void toggleDisplayOnPortal(WebDriver driver) {
		
		cycleAddCategoryObj.toggle_DisplayOnPortal(driver).click();
		
	}
	
	public static void uploadIcon(WebDriver driver) throws InterruptedException, IOException {
		
		cycleAddCategoryObj.button_UploadIcon(driver).click();
		Runtime.getRuntime().exec("C:\\Users\\pmartinez\\Desktop\\automation_files\\upload.exe");
		Thread.sleep(2000);
		//TODO add a check and customizable file name choice for the upload
		
	}
	
	//TODO make this more customizable to be able to choose any store
	public static void selectStore(WebDriver driver) throws InterruptedException {
		
		cycleAddNICIObj.button_FirstStore(driver).click();
		Thread.sleep(2000);
		
	}
	
	public static void dismissAlert(WebDriver driver) {
		
		alert = (new WebDriverWait(driver, 30))
				.until(ExpectedConditions.elementToBeClickable(cycleAddNICIObj.alert_OK(driver)));

		alert.click();
	}
}
