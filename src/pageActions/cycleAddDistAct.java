package pageActions;

import java.io.IOException;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.cycleAddDistObj;
import pageObjects.cycleMgtObj;

public class cycleAddDistAct {

	//verify add new button
	private static WebElement itemNum = null;
	private static WebElement vendor = null;
	private static WebElement upc = null;
	private static WebElement alert = null;
	private static WebElement submit = null;
	private static Select oSelect;
	private static WebElement firstStore = null;
	
	public static void Execute (WebDriver driver, String number, String vendor,
								String category, String upc, String description, String url) throws InterruptedException, IOException {
		
		cycleMgtObj.button_DistVoid(driver).click();
		Thread.sleep(2000);	
		cycleAddDistObj.button_AddNewDist(driver).click();
		if(verify(driver) == true) {
			
			//uploadImage(driver);
			fillItemNum(driver, number);
			fillVendor(driver, vendor);
			selectCategory(driver, category);
			fillUPC(driver, upc);
			fillItemDesc(driver, description);
			Thread.sleep(4000);
			cycleAddDistObj.button_Proceed(driver).click();
			
			firstStore = (new WebDriverWait(driver, 60))
					.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//LABEL[@class='customCB'])[2]")));
			
			//Thread.sleep(7000);
			selectStore(driver);
			submit(driver);
			Thread.sleep(10000);
			driver.navigate().to(url);
			Thread.sleep(3000);
			System.out.println("add distribution void SUCCESS");
			
		} else {
			
			System.out.println("add distribution void FAILED");
			
		}
	}
	
	public static boolean verify(WebDriver driver) {
		
		itemNum = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddDistObj.textbox_ItemNumber(driver)));
		
		vendor = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddDistObj.textbox_Vendor(driver)));
		
		upc = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddDistObj.textbox_UPC(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//TODO concatenate the url with the little number you get for the project to do URL check
		//verify URLs MAKE A HELPER FOR THIS IN TEST CONFIG OR SOMETHING
//		String currURL = driver.getCurrentUrl();
//		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/customer/step1";
		
		//Asserts
		Assert.assertFalse(errorCheck);
//		Assert.assertTrue(currURL.equals(expectedURL));
		
		if(itemNum != null && vendor != null && upc != null) {
			
			return true;
			
		}
		
		return false;
	
	}
	
	public static void uploadImage(WebDriver driver) throws InterruptedException, IOException {
		
		cycleAddDistObj.button_UploadImage(driver).click();
		Runtime.getRuntime().exec("C:\\Users\\pmartinez\\Desktop\\automation_files\\upload.exe");
		Thread.sleep(2000);
		//TODO add a check and customizable file name choice for the upload
		
	}
	
	public static void fillItemNum(WebDriver driver, String num) {
		
		cycleAddDistObj.textbox_ItemNumber(driver).sendKeys(num);
		
	}
	
	public static void fillVendor(WebDriver driver, String vendor) {
		
		cycleAddDistObj.textbox_Vendor(driver).sendKeys(vendor);
		
	}
	
	public static void selectCategory(WebDriver driver, String category) {
		
		oSelect = new Select(cycleAddDistObj.select_Category(driver));
		oSelect.selectByVisibleText(category);
		
	}
	
	public static void fillUPC(WebDriver driver, String upc) {
		
		cycleAddDistObj.textbox_UPC(driver).sendKeys(upc);
		
	}
	
	public static void fillItemDesc(WebDriver driver, String desc) {
		
		cycleAddDistObj.textbox_ItemDesc(driver).sendKeys(desc);
		
	}
	
	//TODO make this more customizable to be able to choose any store
	public static void selectStore(WebDriver driver) throws InterruptedException {
		
		cycleAddDistObj.button_FirstStore(driver).click();
		Thread.sleep(2000);
		
	}
	
	public static void submit(WebDriver driver) {
		
		submit = (new WebDriverWait(driver, 30))
				.until(ExpectedConditions.elementToBeClickable(cycleAddDistObj.button_Submit(driver)));
		
		submit.click();
	}
	
}
