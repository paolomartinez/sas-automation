package pageActions;

import java.io.IOException;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.addCustomerStep1Obj;
import pageObjects.cycleAddPlanogramObj;
import pageObjects.cycleMgtObj;

public class cycleAddPlanogramAct {

	private static WebElement importExcel = null;
	private static WebElement exportExcel = null;
	private static WebElement sweetAlert = null;
	
	//get on the main page for cycle 
	//click planogram compliance
	//click import excel
	//click browse
	//autoit script to upload the short xlsx file
	//click upload
	//wait
	public static void Execute(WebDriver driver) throws InterruptedException, IOException {
		
		//TODO delete this block of code later, just for quick testing purposes
		driver.navigate().to("https://test01.sasretail.com/en/sasretail/activation/cycle-management/847");
		Thread.sleep(3000);
		//end of delete this
		
		cycleMgtObj.button_PlanogramComp(driver).click();
		if(verify(driver) == true) {
			
			cycleAddPlanogramObj.button_Import(driver).click();
			Thread.sleep(3000);
			
			uploadPlanogram(driver);
			Thread.sleep(3000);
			
			//TODO dis the problem area
			cycleAddPlanogramObj.button_Upload(driver).click();
			//Thread.sleep(20000);
			
			sweetAlert = (new WebDriverWait(driver, 20))
					.until(ExpectedConditions.elementToBeClickable(cycleAddPlanogramObj.alert_Success(driver)));
			
			System.out.println("add planogram compliance SUCCESS");
			
		} else {
			
			System.out.println("add planogram compliance FAILED");
			
		}

	}
	
	public static boolean verify(WebDriver driver) {
		
		importExcel = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddPlanogramObj.button_Import(driver)));
		
		exportExcel = (new WebDriverWait(driver, 30))
				.until(ExpectedConditions.elementToBeClickable(cycleAddPlanogramObj.button_Export(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		Assert.assertTrue(!visibleText.contains("error"));
		
		if(importExcel != null && exportExcel != null) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static void uploadPlanogram(WebDriver driver) throws InterruptedException, IOException {
		
		cycleAddPlanogramObj.button_Browse(driver).click();
		Runtime.getRuntime().exec("C:\\Users\\pmartinez\\Desktop\\automation_files\\upload_planogram.exe");
		Thread.sleep(2000);

	}
}
