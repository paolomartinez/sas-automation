package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.cycleAddSurveyObj;
import pageObjects.cycleMgtObj;

public class cycleAddSurveyAct {

	private static WebElement title = null;
	private static Select oSelect;
	private static WebElement proceed = null;
	
	public static void Execute(WebDriver driver, String store, String title, String question, String type, String url) throws InterruptedException {
		
		cycleMgtObj.button_Survey(driver).click();
		Thread.sleep(2000);
		cycleAddSurveyObj.button_AddNewSurvey(driver).click();
		if(verify(driver) == true) {
			
			fillSurvey(driver, store, title);
			Thread.sleep(3000);
			addQuestion(driver, question, type);
			publishSurvey(driver);
			Thread.sleep(3000);
			driver.navigate().to(url);
			Thread.sleep(3000);
			System.out.println("add survey SUCCESS");
		
		} else {
			
			System.out.println("add survey FAILED");
			
		}
	}
	
	public static boolean verify(WebDriver driver) {
		
		title = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleAddSurveyObj.textbox_SurveyTitle(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//Asserts
		Assert.assertFalse(errorCheck);
		
		if(title != null) {
			
			return true;
			
		}
		
		return false;
	
	}
	
	public static void fillSurvey(WebDriver driver, String title, String store) throws InterruptedException {
		
		cycleAddSurveyObj.textbox_SurveyTitle(driver).sendKeys(title);
		addStore(driver, store);
//		proceed = (new WebDriverWait(driver, 10))
//				.until(ExpectedConditions.elementToBeClickable(cycleAddSurveyObj.button_Proceed(driver)));
//		proceed.click();
		
		Thread.sleep(4000);
		cycleAddSurveyObj.button_Proceed(driver).click();
		
	}
	
	//TODO currently all stores get selected, fix this to be customizable in the future
	public static void addStore(WebDriver driver, String store) {
		
		cycleAddSurveyObj.button_StoreSelection(driver).click();
		cycleAddSurveyObj.button_SelectAllStores(driver).click();
		cycleAddSurveyObj.button_SubmitStore(driver).click();
		
	}
	
	public static void addQuestion(WebDriver driver, String question, String type) {
		
		cycleAddSurveyObj.textbox_Question(driver).click();
		cycleAddSurveyObj.textbox_Question(driver).sendKeys(question);
		oSelect = new Select(cycleAddSurveyObj.select_QuestionType(driver));
		oSelect.selectByVisibleText(type);
		cycleAddSurveyObj.button_AddQuestion(driver).click();
		
	}
	
	public static void publishSurvey(WebDriver driver) throws InterruptedException {
		
		cycleAddSurveyObj.button_PublishSurvey(driver).click();
		Thread.sleep(3000);
		cycleAddSurveyObj.button_OK(driver).click();
		
	}
}
