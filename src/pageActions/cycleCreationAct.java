package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.cycleCreationObj;
import pageObjects.cycleMgtObj;

public class cycleCreationAct {

	private static WebElement select_Customer = null;
	
	private static Select oSelect;
	
	public static void Execute(WebDriver driver, String customer, String program, String project,
								String cycleName, String startDate, String endDate) throws InterruptedException {
		
		//leftMenuObj.button_Cycles(driver).click();

		if(verify(driver) == true) {
			
			selectCustomer(driver, customer);
			Thread.sleep(1000);
			selectProgram(driver, program);
			Thread.sleep(1000);
			selectProject(driver, project);
			Thread.sleep(3000);
			createCycle(driver, cycleName, startDate, endDate);
			Thread.sleep(2000);
			System.out.println("cycle creation SUCCESS");
			
		} else {
			
			System.out.println("cycle creation FAILED");
			
		}
	}
	
	public static boolean verify(WebDriver driver) {
		
		select_Customer = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleCreationObj.select_Customer(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs MAKE A HELPER FOR THIS IN TEST CONFIG OR SOMETHING
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/activation/cycle-management/";
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
	
		if(select_Customer != null) {

			return true;
			
		}
	
		return false;
	
	}
	
	public static void selectCustomer(WebDriver driver, String customer) {
		
		cycleCreationObj.select_Customer(driver).click();
		cycleCreationObj.select_Customer(driver).sendKeys(customer);
		cycleCreationObj.select_Customer(driver).sendKeys(Keys.RETURN);
		
	}
	
	
	public static void selectProgram(WebDriver driver, String program) {
	
		cycleCreationObj.select_Program(driver).click();
		cycleCreationObj.select_Program(driver).sendKeys(program);
		cycleCreationObj.select_Program(driver).sendKeys(Keys.RETURN);
	}
	
	public static void selectProject(WebDriver driver, String project) {
		
		oSelect = new Select(cycleCreationObj.select_Project(driver));
		oSelect.selectByVisibleText(project);
		
	}
	
	public static void createCycle(WebDriver driver, String name, String start, String end) throws InterruptedException {
		
		cycleMgtObj.button_AddNewCycle(driver).click();
		Thread.sleep(3000);
		cycleMgtObj.textbox_Name(driver).sendKeys(name);
		cycleMgtObj.textbox_StartDate(driver).sendKeys(start);
		cycleMgtObj.textbox_EndDate(driver).sendKeys(end);
		cycleMgtObj.button_AddInWindow(driver).click();
		
	}
}
