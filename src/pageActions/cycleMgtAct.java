package pageActions;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.cycleMgtObj;
import pageObjects.cycleTeamSchedObj;

public class cycleMgtAct {

	private static WebElement addCycle = null;
	private static WebElement reasonText = null;
	
	public static void Execute(WebDriver driver, String startDate, String dueDate, String teamName, String store,
								String hours, String shiftStart, String shiftEnd, String id, 
								String employee, String isTeamLead) throws InterruptedException {
	
		if(verify(driver) == true) {
						
			createSchedule(driver, teamName, store, startDate, dueDate, shiftStart, shiftEnd, hours);
			Thread.sleep(5000);
		
			createTeam(driver, teamName);
			
			// changes the focus from one tab to the other and grabs the second tab's URL
		    ArrayList<String> tabs2 = new ArrayList<String> (driver.getWindowHandles());
		    driver.switchTo().window(tabs2.get(1));
			String url = driver.getCurrentUrl();
			System.out.println("url now is: " + url);
		    driver.close();
		    driver.switchTo().window(tabs2.get(0));
		    	    
		    driver.navigate().to(url);
			Thread.sleep(4000);
			
			createEmployee(driver, id, employee, dueDate, isTeamLead);
			System.out.println("create cycle SUCCESS");
			
				
		} else {
			
			System.out.println("create cycle FAILED");
			
		}	
	}
	
	public static boolean verify(WebDriver driver) {
		
		addCycle = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleMgtObj.button_AddNewCycle(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//TODO maybe just delete the url verification for all of these
		//verify URLs MAKE A HELPER FOR THIS IN TEST CONFIG OR SOMETHING
//		String currURL = driver.getCurrentUrl();
//		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/customer/step1";
		
		//Asserts
		Assert.assertFalse(errorCheck);
//		Assert.assertTrue(currURL.equals(expectedURL));
		
		if(addCycle != null) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	public static void createSchedule(WebDriver driver, String teamName, String store, 
									String startDate, String dueDate, String shiftStart,
									String shiftEnd, String hours) throws InterruptedException {
		
		cycleMgtObj.button_Schedule(driver).click();
		Thread.sleep(2000);
		cycleTeamSchedObj.button_AddNewSchedule(driver).click();
		Thread.sleep(2000);
		cycleTeamSchedObj.select_Team(driver).sendKeys(teamName);
		cycleTeamSchedObj.select_Store(driver).sendKeys(store);
		cycleTeamSchedObj.textbox_ScheduleDate(driver).sendKeys(startDate);
		cycleTeamSchedObj.textbox_DueDate(driver).sendKeys(dueDate);
		cycleTeamSchedObj.textbox_ScheduledHours(driver).sendKeys(hours);
		cycleTeamSchedObj.textbox_ShiftStart(driver).clear();
		
		String timeStamp = new SimpleDateFormat("hh:mm").format(Calendar.getInstance().getTime());
		
		cycleTeamSchedObj.textbox_ShiftStart(driver).sendKeys(timeStamp);
		cycleTeamSchedObj.textbox_ShiftEnd(driver).clear();
		cycleTeamSchedObj.textbox_ShiftEnd(driver).sendKeys("6:00");
		
		cycleTeamSchedObj.button_SubmitSchedule(driver).click();
		
	}
	
	public static void createTeam(WebDriver driver, String team) throws InterruptedException {
		
		Thread.sleep(2000);
		String xpathExpression = "//A[@target='_blank'][text()='" + team + "']";
		System.out.println(xpathExpression);
		WebElement teamTxt = driver.findElement(By.xpath(xpathExpression));
		teamTxt.click();
		Thread.sleep(2000);
		String fullURL = driver.getCurrentUrl();
		System.out.println("full url in create team: " + fullURL);
	}
	
	public static void createEmployee(WebDriver driver, String id, String employee, 
										String dueDate, String teamLead) throws InterruptedException {
				
		System.out.println("clicking new shift button using hover");
		
		
		Actions builder = new Actions(driver);
		builder.moveToElement(cycleTeamSchedObj.button_AddNewShift(driver)).perform();
		cycleTeamSchedObj.button_AddNewShift(driver).click();
		
		System.out.println("just clicked the add new button");
		Thread.sleep(3000);
		cycleTeamSchedObj.textbox_employeeID(driver).click();
		cycleTeamSchedObj.textbox_employeeID(driver).sendKeys(id);
		
		cycleTeamSchedObj.textbox_DueBy(driver).sendKeys(dueDate);
		
		cycleTeamSchedObj.textbox_employee(driver).click();
		cycleTeamSchedObj.textbox_employee(driver).clear();
		cycleTeamSchedObj.textbox_employee(driver).sendKeys(employee);
		Thread.sleep(3000);
		cycleTeamSchedObj.textbox_employee(driver).sendKeys(Keys.RETURN);

		if(teamLead.toLowerCase().equals("yes")) {
			
			cycleTeamSchedObj.toggle_TeamLead(driver).click();
			
		}
		
		cycleTeamSchedObj.button_SubmitShift(driver).click();
		
		//Assert that employee name is on the page
		/*
		String visibleText = driver.findElement(By.tagName("body")).getText();
		boolean employeeCheck = visibleText.toLowerCase().contains(employee);	
		if(employeeCheck == false) {
			
			System.out.println("Add shift FAILED");
			
		}
		*/
		Thread.sleep(4000);
	}
	
	//TODO this doesn't work because the reason textbox can't be detected
	public static void deleteCycle(WebDriver driver) throws InterruptedException {
		
		//on cycle management home page, click manage icon
		//tap cancel cycle
		//enter reason
		//tap yes
		cycleMgtObj.button_Manage(driver).click();
		Thread.sleep(1000);
		cycleMgtObj.button_CancelCycle(driver).click();
		//Thread.sleep(5000);
		reasonText = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(cycleMgtObj.textbox_Reason(driver)));
		reasonText.click();
		reasonText.sendKeys("hey");
		cycleMgtObj.button_Yes(driver).click();
		Thread.sleep(4000);
		
	}
}
