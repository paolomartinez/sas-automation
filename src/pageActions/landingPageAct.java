package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.landingPageObj;

public class landingPageAct {

	private static WebElement customerSelect = null;
	private static WebElement dateFromSelect = null;
	private static WebElement dateToSelect = null;
	private static WebElement filterButton = null;
	
	public static void Execute(WebDriver driver) throws Exception {
		
		if(verifyLandingPage(driver) == true) {
			System.out.println("landing page verification SUCCESS");
		} else {
			System.out.println("landing page verification FAILED");
		}
	}
	
	public static boolean verifyLandingPage(WebDriver driver) {

		//verify four controls		
		customerSelect = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(landingPageObj.select_Customer(driver)));
		
		dateFromSelect = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(landingPageObj.date_From(driver)));

		dateToSelect = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(landingPageObj.date_To(driver)));

		filterButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(landingPageObj.button_Filter(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);
		
		//verify URL
		String currURL = new String(driver.getCurrentUrl());
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/dashboard";
		
		//Asserts
		Assert.assertTrue(customerSelect != null && dateFromSelect != null 
				&& dateToSelect != null && filterButton != null);
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
		
		//Compare URL to expected URL
		//Check all four controls exist
		//Search for "error" visible text
		if(customerSelect != null && dateFromSelect != null && dateToSelect != null
				&& filterButton != null) {
			if(currURL.equals(expectedURL) && errorCheck == false) {
				return true;
			}
		}
		return false;
	}
	
	
}
