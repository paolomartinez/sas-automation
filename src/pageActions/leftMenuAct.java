package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.leftMenuObj;

public class leftMenuAct {

	private static WebElement customers = null;
	private static WebElement manage_customers = null;
	private static WebElement add_customer = null;
	
	private static WebElement program = null;
	private static WebElement manage_programs = null;
	private static WebElement add_program = null;
	
	private static WebElement projects = null;
	private static WebElement manage_projects = null;
	private static WebElement add_project = null;
	
	private static WebElement cycles = null;
	private static WebElement alerts = null;
	private static WebElement workforce = null;
	
	private static WebElement operations = null;
	private static WebElement schedule = null;
	private static WebElement field_data = null;
	private static WebElement time_exp = null;
	private static WebElement bill_prep = null;
	
	private static WebElement operations_report = null;
	
	private static WebElement payroll = null;
	private static WebElement aggregate = null;
	private static WebElement shift = null;
	private static WebElement cost_center = null;
	private static WebElement expense = null;
	
	public static void Execute(WebDriver driver) throws Exception {
		if(verifyLeftMenu(driver) == true) {
			System.out.println("left menu verification SUCCESS");
		} else {
			System.out.println("left menu verification FAILED");
		}
	}
	
	public static boolean verifyLeftMenu(WebDriver driver) {
		/*
		 * Verify all left menu controls
		 * Verify error not present on page
		 */
		customers = (new WebDriverWait(driver, 20))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Customers(driver)));
		customers.click();
		manage_customers = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_ManageCustomers(driver)));
		add_customer = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_AddCustomer(driver)));
		customers.click();
		
		program = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Program(driver)));
		program.click();
		manage_programs = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_ManagePrograms(driver)));
		add_program = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_AddProgram(driver)));
		program.click();
		
		projects = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Projects(driver)));
		projects.click();
		manage_projects = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_ManageProjects(driver)));
		add_project = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_AddProject(driver)));
		projects.click();
		
		cycles = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Cycles(driver)));

		alerts = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Alerts(driver)));;
		workforce = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Workforce(driver)));

		
		operations = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Operations(driver)));
		operations.click();

		/*
		schedule = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Schedule(driver)));
		field_data = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_FieldDataManagement(driver)));
		time_exp = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_TimeExpense(driver)));
		bill_prep = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_BillPrep(driver)));
		operations.click();
		
		operations_report = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_OperationsReport(driver)));
		
	
		payroll = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_PayrollReport(driver)));
		
		Actions action = new Actions(driver);
		action.moveToElement(payroll).click().perform();
		*/
		/* TODO this is timing out for some reason, take out for now since we don't use it
		aggregate = (new WebDriverWait(driver, 20))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Aggregate(driver)));
		shift = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Shift(driver)));
		cost_center = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_CostCenter(driver)));
		expense = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(leftMenuObj.button_Expense(driver)));
		payroll.click();
		*/
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
				
		//verify URLs
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/dashboard";
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));

		if(errorCheck == false &&  
				customers != null &&
				manage_customers != null &&
				add_customer != null &&
				
				program != null &&
				manage_programs != null &&
				add_program != null &&
				
				projects != null &&
				manage_projects != null &&
				add_project != null &&
				
				cycles != null &&
				alerts != null &&
				workforce != null) {
/*				
				operations != null &&
				schedule != null &&
				field_data != null &&
				time_exp != null &&
				bill_prep != null &&
				
				operations_report != null &&
				
				payroll != null &&
				aggregate != null &&
				shift != null &&
				cost_center != null &&
				expense != null) { 
*/								
			return true;
		}
		
		return false;
	}
	
}
