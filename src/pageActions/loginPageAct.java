package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.loginPageObj;

public class loginPageAct {

	private static WebElement userField = null;
	private static WebElement passwordField = null;
	private static WebElement loginButton = null;
	private static WebElement loginError = null;
	
	public static void Execute(WebDriver driver, String sUserName, String sPassword) throws Exception {	
			
		if(verifyLoginPage(driver)) {
			fillUser(driver, sUserName);
			fillPassword(driver, sPassword);
			submitLogin(driver);
			System.out.println("login execution COMPLETED");
		} else {
			System.out.println("login execution FAILED");
		}
	}

	public static void fillUser(WebDriver driver, String sUserName) {
		loginPageObj.textbox_UserName(driver).sendKeys(sUserName);
	}
	
	
	public static void fillPassword(WebDriver driver, String sPassword) {
		loginPageObj.textbox_Password(driver).sendKeys(sPassword);
	}
	
	
	public static void submitLogin(WebDriver driver) throws InterruptedException{
		Thread.sleep(1000);
		loginPageObj.button_LogIn(driver).click();
		loginPageObj.button_LogIn(driver).click();
	}

	public static boolean verifyLoginPage(WebDriver driver) {		
		
		//verify URL
		String currURL = new String(driver.getCurrentUrl());
		String expectedURL = testConfig.URL;
		
		//verify three controls
		userField = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginPageObj.textbox_UserName(driver)));
		
		passwordField = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginPageObj.textbox_Password(driver)));
		
		loginButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginPageObj.button_LogIn(driver)));

		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);
		
		//Asserts
		Assert.assertTrue(userField != null && passwordField != null && loginButton != null);
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
				
		//Compare URL to expected URL
		//Check all three controls exist
		//Search for "error" visible text
		if(userField != null && passwordField != null && loginButton != null 
				&& errorCheck == false) {
			//test this thing really quick
			if(currURL.equals(expectedURL)) {
				return true;
			}
		}

		System.out.println("error text was not found");
		return false;		
	}
	
	public static boolean verifyErrorMessage(WebDriver driver) {
		
		//verify control
		loginError = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(loginPageObj.errorMessage(driver)));
		//Assert
		Assert.assertTrue(loginError != null);
		
		if(loginError != null) {
			System.out.println("invalid email/password message present");
			return true;
		}
		System.out.println("cannot find error message");
		return false;
	}
	
} 


