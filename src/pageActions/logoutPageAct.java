package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.dataHandling;
import pageObjects.loginPageObj;
import pageObjects.logoutPageObj;
import common.testConfig;

public class logoutPageAct {

	private static WebElement settingsArrow = null;
	private static WebElement logoutButton = null;
	private static WebElement userField = null;
	private static WebElement passwordField = null;
	private static WebElement loginButton = null;
	
	public static void Execute(WebDriver driver, String user, int i) throws Exception {
		
		if(verifyLogoutPage(driver, user, i) == true) {
			System.out.println("user verification SUCCESS");
		} else {
			System.out.println("user verification FAILED");
		}
	}
	
	public static boolean verifyLoginPage_NoURL(WebDriver driver) {
		//verify three controls
				userField = (new WebDriverWait(driver, 10))
						.until(ExpectedConditions.elementToBeClickable(loginPageObj.textbox_UserName(driver)));
				
				passwordField = (new WebDriverWait(driver, 10))
						.until(ExpectedConditions.elementToBeClickable(loginPageObj.textbox_Password(driver)));
				
				loginButton = (new WebDriverWait(driver, 10))
						.until(ExpectedConditions.elementToBeClickable(loginPageObj.button_LogIn(driver)));

				
				//verify error not present on page
				String visibleText = driver.findElement(By.tagName("body")).getText();
				String errorText = "error";
				boolean errorCheck = visibleText.toLowerCase().contains(errorText);
				
				//Asserts
				Assert.assertTrue(userField != null && passwordField != null && loginButton != null);
				Assert.assertFalse(errorCheck);
						
				//Compare URL to expected URL
				//Check all three controls exist
				//Search for "error" visible text
				if(userField != null && passwordField != null && loginButton != null 
						&& errorCheck == false) {
					//test this thing really quick
					return true;
				}

				System.out.println("error text was not found");
				return false;		
	}
	
	public static boolean verifyLogoutPage(WebDriver driver, String user, int index) throws Exception {		

		landingPageAct.verifyLandingPage(driver);
		
		//verify correct user
		String cellData = dataHandling.getCellData(index, 1);
		String currUser = cellData.split("\\@")[0].toUpperCase();
		String expectedUser = user;
		if(!currUser.equals(expectedUser)) return false; 
		
		//verify landing page controls
		settingsArrow = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(logoutPageObj.button_SettingsArrow(driver)));
		Assert.assertTrue(settingsArrow != null);
		settingsArrow.click();
		
		logoutButton = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(logoutPageObj.button_logOut(driver)));
		Assert.assertTrue(logoutButton != null);
		logoutButton.click();
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);
		Assert.assertFalse(errorCheck);
		
		//Do this after logging out because we'll be back on login page
		if(loginPageAct.verifyLoginPage(driver) && errorCheck == false) {
			
			//GOOD SECURITY TEST
			//in here, navigate browser to go back to the dashboard link https://test01.sasretail.com/en/sasretail/dashboard
			//make sure dashboard controls aren't there tho
			String url = testConfig.URL + "dashboard";
			driver.navigate().to(url);
			//driver.navigate().to("https://test01.sasretail.com/en/sasretail/dashboard");
			boolean onLoginPage = logoutPageAct.verifyLoginPage_NoURL(driver);
			if(onLoginPage == true) return true;
		}
		
		return false;
	}
}
