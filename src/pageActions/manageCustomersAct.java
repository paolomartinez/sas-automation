package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.leftMenuObj;
import pageObjects.manageCustomersObj;

public class manageCustomersAct {
	
	private static WebElement activeCustomers = null;
	private static WebElement draftCustomers = null;
	private static WebElement archivedCustomers = null;
	private static WebElement addCustomer = null;
	private static WebElement table = null;
	private static WebElement reasonTextbox = null;
	
	public static void Execute(WebDriver driver) throws Exception {
		
		leftMenuObj.button_Customers(driver).click();
		leftMenuObj.button_ManageCustomers(driver).click();
			
		if(verifyManageCustomers(driver) == true) {
			System.out.println("manage customers subpage verification SUCCESS");
		} else {
			System.out.println("manage customers subpage verification FAILED");
		}
		
	}
	
	public static boolean verifyManageCustomers(WebDriver driver) {
		
		table = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageCustomersObj.table_CustomerTable(driver)));
		
		activeCustomers = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(manageCustomersObj.button_ActiveCustomers(driver)));
			
		draftCustomers = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(manageCustomersObj.button_DraftCustomers(driver)));
		
		archivedCustomers = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(manageCustomersObj.button_ArchivedCustomers(driver)));

		addCustomer = (new WebDriverWait(driver, 10))
				.until(ExpectedConditions.elementToBeClickable(manageCustomersObj.button_AddCustomer(driver)));
/*		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
*/
		//verify URLs
		String currURL = driver.getCurrentUrl();
		String expectedURL = testConfig.URL + "sasretail/customer/management/active";
		
		//Asserts
//		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
	
		if( table != null && activeCustomers != null && draftCustomers != null && archivedCustomers != null
				&& addCustomer != null) {
			return true;
		} 

		return false;
	}
	
	//verify it's in archived customers
	public static void deleteCustomer(WebDriver driver, String customer) throws InterruptedException {
		
		//navigate to Manage Customers, search for customer in active customers
		String url = testConfig.URL + "sasretail/customer/management/active";
		driver.navigate().to(url);
		//driver.navigate().to("https://test01.sasretail.com/en/sasretail/customer/management/active");
		Thread.sleep(5000);
		
		/*TODO removing verification because we just want to delete all the customers we created
		 * we don't care about verifying which ones got deleted because the test will only delete
		 * as many as are created
		 */
		/*
		String visibleText = driver.findElement(By.tagName("body")).getText();
		System.out.println("visible text is: " + visibleText);
		Assert.assertTrue(visibleText.contains(customer));
		*/
		
		//archive customer
		manageCustomersObj.button_Manage(driver).click();
		manageCustomersObj.button_Archive(driver).click();
		Thread.sleep(3000);
	
		//provide reason for archive, submit
		manageCustomersObj.button_Yes(driver).click();
		reasonTextbox = (new WebDriverWait(driver, 60))
				.until(ExpectedConditions.elementToBeClickable(manageCustomersObj.textbox_Reason(driver)));
		reasonTextbox.sendKeys("blah");
		Thread.sleep(3000);
		manageCustomersObj.button_Submit(driver).click();
		Thread.sleep(4000);
		
		//verify customer is in archived customers
		manageCustomersObj.button_ArchivedCustomers(driver).click();
		Thread.sleep(5000);
		
		// TODO removing verification so that everything just gets deleted 
		/*
		String archivedText = driver.findElement(By.tagName("body")).getText();	
		Assert.assertTrue(archivedText.contains(customer));
		*/
	}
	
}
