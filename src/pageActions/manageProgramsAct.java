package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.leftMenuObj;
import pageObjects.manageProgramsObj;

public class manageProgramsAct {

	private static WebElement activePrograms = null;
	private static WebElement archivedPrograms = null;
	private static WebElement selectCustomer = null;
	private static WebElement table = null;
	
	public static void Execute(WebDriver driver) throws Exception {
		
		leftMenuObj.button_Program(driver).click();
		leftMenuObj.button_ManagePrograms(driver).click();
		
		if(verifyManagePrograms(driver) == true) {
			System.out.println("manage programs subpage verification SUCCESS");
		} else {
			System.out.println("manage programs subpage verification FAILED");
		}
		
	}
	
	public static boolean verifyManagePrograms(WebDriver driver) {
		
		table = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProgramsObj.table_ProgramTable(driver)));
		
		activePrograms = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProgramsObj.button_ActivePrograms(driver)));
		
		archivedPrograms = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProgramsObj.button_ArchivedPrograms(driver)));
		
		selectCustomer = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProgramsObj.select_Customer(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/program/management/active";
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
		
		if(table != null && activePrograms != null && archivedPrograms != null && selectCustomer != null
				&& errorCheck == false) {
			return true;
		}
		
		return false;
	}
	
	public static void deleteProgram(WebDriver driver, String programName) throws InterruptedException {
		
		//In Manage Programs, type in program name to be archived
		String url = testConfig.URL + "program/management/active";
		driver.navigate().to(url);
		//driver.navigate().to("https://test01.sasretail.com/en/sasretail/program/management/active");
		manageProgramsObj.textbox_SearchPrograms(driver).click();
		manageProgramsObj.textbox_SearchPrograms(driver).sendKeys(programName);
		Thread.sleep(2000);
		
		//archive the program
		manageProgramsObj.button_ManageMostRecentProg(driver).click();
		manageProgramsObj.button_Archive(driver).click();
		Thread.sleep(2000);
		
		manageProgramsObj.button_Yes(driver).click();
		manageProgramsObj.textbox_Reason(driver).sendKeys("blahblah");
		manageProgramsObj.button_Submit(driver).click();
		Thread.sleep(5000);
		
		manageProgramsObj.button_ArchivedPrograms(driver).click();
		Thread.sleep(3000);
		
		//verify program is in archived programs
		String visibleText = driver.findElement(By.tagName("body")).getText();
		Assert.assertTrue("Text not found!", visibleText.contains(programName));
		System.out.println("archive program SUCCESS");
	}
}
