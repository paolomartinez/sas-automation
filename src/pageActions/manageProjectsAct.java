package pageActions;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import common.testConfig;
import pageObjects.leftMenuObj;
import pageObjects.manageCustomersObj;
import pageObjects.manageProjectsObj;

public class manageProjectsAct {

	private static WebElement activeProjects = null;
	private static WebElement draftProjects = null;
	private static WebElement archivedProjects = null;
	private static WebElement addProject = null;
	private static WebElement table = null;
	
	public static void Execute(WebDriver driver) throws Exception {
		
		leftMenuObj.button_Projects(driver).click();
		leftMenuObj.button_ManageProjects(driver).click();
			
		if(verifyManageProjects(driver) == true) {
			
			System.out.println("manage projects subpage verification SUCCESS");
			
		} else {
			
			System.out.println("manage projects subpage verification FAILED");
			
		}
		
	}
	
	public static boolean verifyManageProjects(WebDriver driver) {
		
		activeProjects = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProjectsObj.button_ActiveProjects(driver)));
		
		draftProjects = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProjectsObj.button_DraftProjects(driver)));
		
		archivedProjects = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProjectsObj.button_ArchivedProjects(driver)));
		
		addProject = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProjectsObj.button_AddProject(driver)));
		
		table = (new WebDriverWait(driver, 500))
				.until(ExpectedConditions.elementToBeClickable(manageProjectsObj.table_ProjectTable(driver)));
		
		//verify error not present on page
		String visibleText = driver.findElement(By.tagName("body")).getText();
		String errorText = "error";
		boolean errorCheck = visibleText.toLowerCase().contains(errorText);	
		
		//verify URLs
		String currURL = driver.getCurrentUrl();
		String expectedURL = "https://loadtest.sasretail.com/en/sasretail/project/management/active";
		
		//Asserts
		Assert.assertFalse(errorCheck);
		Assert.assertTrue(currURL.equals(expectedURL));
		
		if(activeProjects != null && draftProjects != null &&
				archivedProjects != null && addProject != null && table != null
				&& errorCheck == false) {
			return true;
		}
		
		return false;
	}
	
	public static void deleteProject(WebDriver driver, String project, String date) throws InterruptedException {
		
		//navigate to Manage Projects, search for project in active projects
		String url = testConfig.URL + "project/management/active";
		driver.navigate().to(url);
		//driver.navigate().to("https://test01.sasretail.com/en/sasretail/project/management/active");
		manageProjectsObj.textbox_SearchProjects(driver).click();
		manageProjectsObj.textbox_SearchProjects(driver).sendKeys(project);
		Thread.sleep(3000);
		
		//pause project
		manageProjectsObj.button_Manage(driver).click();
		manageProjectsObj.button_PauseProject(driver).click();
		Thread.sleep(2000);
		
		//provide reason and date for cancellation, submit
		manageProjectsObj.textbox_DateEffective(driver).sendKeys(date);
		manageProjectsObj.textbox_Reason(driver).click();
		manageProjectsObj.textbox_Reason(driver).sendKeys("blah");
		
		manageProjectsObj.button_Submit(driver).click();
		Thread.sleep(3000);
		
		//verify project is paused
		manageProjectsObj.textbox_SearchProjects(driver).click();
		manageProjectsObj.textbox_SearchProjects(driver).sendKeys(project);
		Thread.sleep(4000);
		Assert.assertTrue(manageProjectsObj.icon_Paused(driver) != null);
	}
}
