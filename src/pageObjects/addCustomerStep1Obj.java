package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addCustomerStep1Obj {

	private static WebElement element = null;
	
	//Fill Data Section
	public static WebElement button_UploadLogo(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-cloud-storage10'])[2]"));
		
		return element;
	}
	
	public static WebElement picture_customerLogo(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//IMG[@alt='customer logo'])[1]"));
		
		return element;
	}
	
	public static WebElement textbox_CustomerName(WebDriver driver) {
		
		element = driver.findElement(By.name("customerName"));
		
		return element;
	}
	
	public static WebElement select_CustomerType(WebDriver driver) {
		
		element = driver.findElement(By.name("customerType"));
		
		return element;
	}
	
	public static WebElement select_ParentCompany(WebDriver driver) {
		
		element = driver.findElement(By.name("parent"));
		
		return element;
	}
	
	public static WebElement textbox_Address(WebDriver driver) {
		
		element = driver.findElement(By.name("address"));
		
		return element;
	}
	
	public static WebElement select_Country(WebDriver driver) {
		
		element = driver.findElement(By.name("country"));
		
		return element;
	}
	
	public static WebElement select_State(WebDriver driver) {
		
		element = driver.findElement(By.name("state"));
		
		return element;
	}
	
	public static WebElement textbox_City(WebDriver driver) {
		
		element = driver.findElement(By.name("city_obj"));
		
		return element;
	}
	
	public static WebElement textbox_Zip(WebDriver driver) {
		
		element = driver.findElement(By.name("zip"));
		
		return element;
	}
	
	
	//Account Payable section
	public static WebElement textbox_AccountContact(WebDriver driver) {
		
		element = driver.findElement(By.name("account_payable_contact_name"));
		
		return element;
	}
	
	public static WebElement textbox_PrimaryPhone(WebDriver driver) {
		
		element = driver.findElement(By.name("primary_phone_no"));
		
		return element;
	}
	
	public static WebElement textbox_SecondaryPhone(WebDriver driver) {
		
		element = driver.findElement(By.name("secondary_phone_no"));
		
		return element;
	}
	
	public static WebElement textbox_Email(WebDriver driver) {
		
		element = driver.findElement(By.name("email"));
		
		return element;
	}
	
	//Payment Terms section
	public static WebElement select_PaymentTerms(WebDriver driver) {
		
		element = driver.findElement(By.name("payment_terms"));
		
		return element;
	}
	
	public static WebElement textbox_DBNumber(WebDriver driver) {
		
		element = driver.findElement(By.name("duns_given_id"));
		
		return element;
	}
	
	public static WebElement textbox_CompanyTaxID(WebDriver driver) {
		
		element = driver.findElement(By.name("tax_id"));
		
		return element;
	}
	
	public static WebElement textbox_CostCenter(WebDriver driver) {
		
		element = driver.findElement(By.name("finance_id"));
		
		return element;
	}
	
	public static WebElement switch_CreditLevel(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[@class='labelText'][text()='Credit Level Check?']"));
		
		return element;
	}
	
	public static WebElement textbox_CreditAmount(WebDriver driver) {
		
		element = driver.findElement(By.name("credit_amount"));
		
		return element;
	}
	
	public static WebElement switch_PreBill(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[@class='labelText'][text()='Pre-bill']"));
		
		return element;
	}
	
	public static WebElement textbox_PreBillAmt(WebDriver driver) {
		
		element = driver.findElement(By.name("prebill_percent"));
		
		return element;
	}
	
	
	//More Add New buttons
	public static WebElement button_BillingInfo(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//BUTTON[@type='button'][text()='Add New'][text()='Add New'])[1]"));
		
		return element;
	}
	
	
	public static WebElement textbox_BillingName(WebDriver driver) {
		
		element = driver.findElement(By.name("person_name"));
		
		return element;
		
	}
	
	public static WebElement textbox_PrimaryContactTitle(WebDriver driver) {
		
		element = driver.findElement(By.name("title"));
		
		return element;
		
	}
	
	
	public static WebElement textbox_BillingEmail(WebDriver driver) {
		
		element = driver.findElement(By.name("email"));
		
		return element;
		
	}
	
	public static WebElement textbox_InvoiceSpecificEmail(WebDriver driver) {
		
		element = driver.findElement(By.name("invoice_specific_email"));
		
		return element;
		
	}
	
	public static WebElement textbox_PhoneNumber(WebDriver driver) {
		
		element = driver.findElement(By.name("phone_number"));

		return element;
		
	}
	
	public static WebElement button_AddBillingContact(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//BUTTON[@type='submit'])[4]"));
		
		return element;
		
	}
	
	public static WebElement button_PrimaryContacts(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//BUTTON[@type='button'][text()='Add New'][text()='Add New'])[2]"));
		
		return element;
		
	}
	
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageBtnContainer col-xs-12']"));
		
		return element;
		
	}

	public static WebElement textbox_Department(WebDriver driver) {
		
		element = driver.findElement(By.name("department"));
		
		return element;
		
	}

	
	public static WebElement textbox_BillingInfoTitle(WebDriver driver) {
		
		element = driver.findElement(By.name("text"));
		
		return element;
	}

	public static WebElement button_DeleteBillingInfo(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-cross31'])[2]"));
		
		return element;
		
	}
	
	public static WebElement textbox_PrimaryContactEmail(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='email'])[4]"));
		
		return element;
		
	}
	
	public static WebElement button_DeletePrimaryContact(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-cross31'])[3]"));
		
		return element;
		
	}
	
	public static WebElement button_ConfirmDelete(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@class='confirm'][text()='Yes, delete Primary Contact!']"));
		
		return element;
		
	}
	
	public static WebElement button_AutocompleteCity(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//A[@href='']"));
		
		return element;
		
	}
	
}
