package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addCustomerStep2Obj {

	private static WebElement element = null;
	
	public static WebElement button_ImportStores(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Import Stores']"));
		
		return element;
		
	}
	
	public static WebElement linktext_DownloadTemplate(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Click here to download the Template"));
		
		return element;
		
	}
	
	public static WebElement button_ChooseFiles(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@class='btn cta gray']"));
		
		return element;
		
	}
	
	//NOTE THE UPLOAD BUTTON ON IMPORT STORES DISAPPEARS????
	public static WebElement button_Upload(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//BUTTON[@type='button'])[14]"));
		
		return element;
		
	}
	
	public static WebElement button_AddStore(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-plus79'])[1]"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreName(WebDriver driver) {
		
		element = driver.findElement(By.name("name"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreNumber(WebDriver driver) {
		
		element = driver.findElement(By.name("number"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreAddress(WebDriver driver) {
		
		element = driver.findElement(By.name("address"));
		
		return element;
		
	}
	
	public static WebElement select_StoreCountry(WebDriver driver) {
		
		element = driver.findElement(By.name("storeCountry"));
		
		return element;
		
	}
	
	public static WebElement select_StoreState(WebDriver driver) {
		
		element = driver.findElement(By.name("storeRegion"));
		
		return element;
		
	}
	
	//NOTE: This is not visible/clickable until state is selected
	public static WebElement select_StoreCity(WebDriver driver) {
		
		element = driver.findElement(By.name("city"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreZip(WebDriver driver) {
		
		element = driver.findElement(By.name("zip"));
		
		return element;
		
	}
	
	public static WebElement textbox_StorePhone(WebDriver driver) {
		
		element = driver.findElement(By.name("phone"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreEmail(WebDriver driver) {
		
		element = driver.findElement(By.name("email"));
		
		return element;
		
	}
	
	public static WebElement button_AddNew(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement button_ImportCategories(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Import Categories']"));
		
		return element;
		
	}
	
	public static WebElement button_AddCategory(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-plus79'])[2]"));
		
		return element;
		
	}
	
	public static WebElement textbox_CategoryNum(WebDriver driver) {
		
		element = driver.findElement(By.name("number"));
		
		return element;
		
	}
	
	public static WebElement textbox_CategoryName(WebDriver driver) {
		
		element = driver.findElement(By.name("name"));
		
		return element;
		
	}
	
	public static WebElement toggle_CategoryPhotoReq(WebDriver driver) {
		
		element = driver.findElement(By.name("is_photo_required"));
		
		return element;
		
	}
	
	public static WebElement button_SubmitCategory(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction202']"));
		
		return element;
		
	}
	
	public static WebElement button_AutocompleteCity(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//A[@href=''])[3]"));
		
		return element;
		
	}
	
	public static WebElement button_OK(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@class='confirm'][text()='OK']"));
		
		return element;
		
	}
}
