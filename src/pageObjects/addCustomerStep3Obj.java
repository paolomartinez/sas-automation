package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addCustomerStep3Obj {

	//scan page for all the cell data from the excel sheet
	//similar logic to error check?
	
	private static WebElement element = null;
	
	public static WebElement button_Documents(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[@class='hidden-xs ng-binding'][text()='Documents (0)']"));
		
		return element;
		
	}
	
	public static WebElement button_Cancel(WebDriver driver) {
		
		//this xpath probably is the problem
		element = driver.findElement(By.xpath("(//DIV[@class='col-sm-4'])[1]"));
				
		return element;
	}
	
	public static WebElement button_SaveAsDraft(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Save as Draft']"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Submit']"));
		
		return element;
		
	}

	/*
	public static WebElement alert_Error(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//H2[text()='Error!']"));
		
		return element;
		
	}*/
	
	public static WebElement button_Ok(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@class='confirm'][text()='OK']"));
		
		return element;
		
	}
	
	public static WebElement button_BackSummary(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='flaticon-edit45']"));
		
		return element;
		
	}
}
