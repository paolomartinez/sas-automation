package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addCustomerStep4Obj {

	private static WebElement element = null;
	
	public static WebElement label_CustomerID(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//LABEL[text()='Customer ID:']"));
		
		return element;
		
	}
	
	public static WebElement label_Customer(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//LABEL[text()='Customer:']"));
		
		return element;
		
	}
	
	public static WebElement button_Back(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction129']"));
		
		return element;
		
	}
	
}
