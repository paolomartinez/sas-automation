package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProgramStep1Obj {

	private static WebElement element = null;
	
	public static WebElement select_CustomerName(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='formdata.clientId']"));
		
		return element;
		
	}
	
	public static WebElement text_ProgramName(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='program.name']"));
		
		return element;
		
	}
	
	public static WebElement select_StartDate(WebDriver driver) {
		
		element = driver.findElement(By.name("program.start_date"));
		
		return element;
		
	}
	
	public static WebElement select_EndDate(WebDriver driver) {
		
		element = driver.findElement(By.name("program.end_date"));
		
		return element;
		
	}
	
	public static WebElement toggle_TeamBasedProgram(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[@class='switchery switchery-default']"));
		
		return element;
		
	}
	
	public static WebElement select_NumTeams(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SELECT[@data-ng-disabled='!program.team_based || disableTeamSizeSelector'])[1]"));
		
		return element;
		
	}
	
	public static WebElement select_AvgTeamSize(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SELECT[@data-ng-disabled='!program.team_based || disableTeamSizeSelector'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageBtnContainer col-xs-12']"));
		
		return element;
		
	}
	
}
