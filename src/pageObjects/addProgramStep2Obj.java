package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProgramStep2Obj {

	private static WebElement element = null;
	
	public static WebElement button_AddNewTeam(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@data-ng-click='addTeam()'][text()='Add New team']"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreSearch(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='team.searchString']"));
		
		return element;
		
	}
	
	public static WebElement textbox_StoreZip(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='team.originZip']"));
		
		return element;
		
	}
	
	public static WebElement select_StoreRadius(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='team.radius']"));
		
		return element;
		
	}
	
	public static WebElement button_Search(WebDriver driver) {
		
		element = driver.findElement(By.className("btn-dark-gray-2"));
		
		return element;
		
	}
	
	public static WebElement button_SelectAll(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[@class='text'][text()='Select All']"));
		
		return element;
		
	}
	
	public static WebElement button_AssignToTeam(WebDriver driver) { 
		
		element = driver.findElement(By.xpath("//I[@class='flaticon-keyboard53']"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageBtnContainer col-xs-12']"));
		
		return element;
		
	}
	
}
