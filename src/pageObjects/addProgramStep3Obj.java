package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProgramStep3Obj {
	
	private static WebElement element = null;

	public static WebElement select_RoleType(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SELECT[@data-ng-model='member.role'])"));
		
		return element;
		
	}
	
	public static WebElement select_AssignTo(WebDriver driver) {
		
		element = driver.findElement(By.name("member.employee_obj"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageBtnContainer col-xs-12']"));
		
		return element;
		
	}
	
	public static WebElement select_Role(WebDriver driver, String path) {
		
		element = driver.findElement(By.xpath(path));
		
		return element;
		
	}
	
	public static WebElement textbox_Employee(WebDriver driver, String path) {
		
		element = driver.findElement(By.xpath(path));
		
		return element;
		
	}
	
}
