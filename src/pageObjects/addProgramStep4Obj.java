package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProgramStep4Obj {

	private static WebElement element;
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//BUTTON[@type='submit'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_Cancel(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@data-ng-click='cancel()']"));
		
		return element;
		
	}
	
	public static WebElement label_Program(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//LABEL[text()='Program:']"));
		
		return element;
		
	}
	
	public static WebElement label_ProgramID(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//LABEL[text()='Program Id:']"));
		
		return element;
		
	}
	
	public static WebElement button_Back(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction129']"));
		
		return element;
		
	}
}
