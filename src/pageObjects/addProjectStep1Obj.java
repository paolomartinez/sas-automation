package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProjectStep1Obj {

	private static WebElement element = null;
	
	public static WebElement textbox_ProjectName(WebDriver driver) {
		
		element = driver.findElement(By.name("projectName"));
		
		return element;
		
	}
	
	public static WebElement textbox_ProjectID(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='project.customer_given_id']"));
		
		return element;
		
	}
	
	public static WebElement select_Customer(WebDriver driver) {
		
		element = driver.findElement(By.name("projectCustomer"));
		
		return element;
		
	}
	
	public static WebElement select_Program(WebDriver driver) {
		
		element = driver.findElement(By.name("projectProgram"));
		
		return element;
		
	}
	
	public static WebElement button_AddProgram(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='ADD new program']"));
		
		return element;
		
	}
	
	public static WebElement select_ProjectType(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='project.type']"));
		
		return element;
		
	}
	
	public static WebElement select_StartDate(WebDriver driver) {
		
		element = driver.findElement(By.name("projectStartDate"));
		
		return element;
		
	}
	
	public static WebElement select_EndDate(WebDriver driver) {
		
		element = driver.findElement(By.name("projectEndDate"));
		
		return element;
		
	}
	
	public static WebElement button_CustomField(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Need Custom Field?']"));
		
		return element;
		
	}
	
	public static WebElement toggle_PictureReq(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[1]"));
		
		return element;
		
	}
	
	public static WebElement toggle_AccessibleToClient(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[2]"));
		
		return element;
		
	}
	
	public static WebElement toggle_MerchandiserMailing(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[3]"));
		
		return element;
		
	}
	
	public static WebElement toggle_ReportTime(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[4]"));
		
		return element;
		
	}
	
	public static WebElement toggle_HighLevelMail(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[5]"));
		
		return element;
		
	}
	
	public static WebElement button_AddMember(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Add New Member']"));
		
		return element;
		
	}
	
	public static WebElement select_Role(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='member.role']"));
		
		return element;
		
	}
	
	public static WebElement textbox_MemberName(WebDriver driver) {
		
		element = driver.findElement(By.name("member_name"));
		
		return element;
		
	}
	
	public static WebElement button_Save(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction202']"));
		
		return element;
		
	}
}
