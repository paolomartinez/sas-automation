package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProjectStep2Obj {

	private static WebElement element = null;
	
	// Services
	public static WebElement textbox_ServiceType(WebDriver driver) {

		element = driver.findElement(By.xpath("//input[@value='Choose one or more services']"));
		
		return element;
		
	}
	
	// Schedule 
	public static WebElement select_Occurrence(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='project.schedule_cycle']"));
		
		return element;
		
	}
	
	public static WebElement textbox_HoursPerVisit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='number'])[1]"));
		
		return element;
		
	}
	
	public static WebElement textbox_ShiftStartTime(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='text'])[3]"));
		
		return element;
		
	}
	
	public static WebElement textbox_ShiftEndTime(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='text'])[4]"));
		
		return element;
		
	}
	
	public static WebElement button_Sunday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Sunday']"));
		
		return element;
		
	}
	
	public static WebElement button_Monday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Monday']"));
		
		return element;
		
	}
	
	public static WebElement button_Tuesday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Tuesday']"));
		
		return element;
		
	}
	
	public static WebElement button_Wednesday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Wednesday']"));
		
		return element;
		
	}
	
	public static WebElement button_Thursday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Thursday']"));
		
		return element;
		
	}
	
	public static WebElement button_Friday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Friday']"));
		
		return element;
		
	}
	
	public static WebElement button_Saturday(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='Saturday']"));
		
		return element;
		
	}
	
	public static WebElement button_AddLocations(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Add Locations']"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction202']"));
		
		return element;
		
	}
	
	public static WebElement button_firstStore(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//LABEL[@class='customCB'])[1]"));
		
		return element;
		
	}
	
	public static WebElement button_Import(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@class='fileInput ng-pristine ng-untouched ng-valid ng-isolate-scope']"));
		
		return element;
		
	}
	
	public static WebElement button_Export(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-share53 rotate180']"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	
}
