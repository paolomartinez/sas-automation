package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProjectStep3Obj {

	private static WebElement element = null;
	
	public static WebElement select_Client(WebDriver driver) {
		
		element = driver.findElement(By.name("terms"));
		
		return element;
		
	}
	
	public static WebElement select_BillingContact(WebDriver driver) {
		
		element = driver.findElement(By.name("qterm"));
		
		return element;
		
	}
	
	public static WebElement select_LOAContact(WebDriver driver) {
		
		element = driver.findElement(By.name("loacontact"));
		
		return element;
		
	}
	
	public static WebElement button_Add(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-plus79'])[1]"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction202']"));
		
		return element;
		
	}
	
	public static WebElement toggle_SameBill(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[1]"));
		
		return element;
		
	}
	
	public static WebElement toggle_BillOvertime(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[2]"));
		
		return element;
		
	}
	
	public static WebElement toggle_BillExpense(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[3]"));
		
		return element;
		
	}
	
	public static WebElement toggle_HolidayBilling(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[4]"));
		
		return element;
		
	}
	
	public static WebElement toggle_VacationBilling(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[5]"));
		
		return element;
		
	}
	
	public static WebElement toggle_SickBilling(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SPAN[@class='switchery switchery-default'])[6]"));
		
		return element;
		
	}
	
	public static WebElement select_RateType(WebDriver driver) {
		
		element = driver.findElement(By.name("ratetype"));
		
		return element;
		
	}
	
	public static WebElement select_PricingType(WebDriver driver) {
		
		element = driver.findElement(By.name("pricingtype"));
		
		return element;
		
	}
	
	public static WebElement textbox_BillingRate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='data[$index].billingRate']"));
		
		return element;
		
	}
	
	public static WebElement textbox_HourlyRate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='data[$index].typePerVisit']"));
		
		return element;
		
	}
	
	public static WebElement button_AddRates(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-plus79'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_autocompleteClient(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//A[@href='']"));
		
		return element;
		
	}
	
	public static WebElement select_BillingTerms(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='data[$index].billingtermstype']"));
		
		return element; 
		
	}
}
