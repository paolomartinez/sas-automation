package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class addProjectStep4Obj {

	private static WebElement element = null;
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Submit']"));
		
		return element;
		
	}
	
	public static WebElement button_Cancel(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Cancel']"));
		
		return element;
		
	}
	
	public static WebElement button_Save(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Save']"));
		
		return element;
		
	}
	
	public static WebElement label_ProjectID(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//LABEL[text()='Project ID:']"));
		
		return element;
		
	}
}
