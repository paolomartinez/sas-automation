package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cycleAddCategoryObj {

	private static WebElement element = null;
	
	public static WebElement button_AddNewCategory(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Add New']"));
		
		return element;
		
	}
	
	public static WebElement select_Category(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='categoryReset.category']"));
		
		return element;
		
	}
	
	public static WebElement textbox_PlanogramID(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='categoryReset.planogram_id']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Size(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='categoryReset.est_size_numerator']"));
		
		return element;
		
	}
	
	public static WebElement select_SizeType(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='categoryReset.est_size_unit']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Hours(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='formdata.selectedHours']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Minutes(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='formdata.selectedMinutes']"));
		
		return element;
		
	}
	
	public static WebElement toggle_DisplayOnPortal(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[@class='switchery switchery-default']"));
		
		return element;
		
	}
	
	public static WebElement button_UploadIcon(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='file'])[1]"));
		
		return element;
		
	}
	
	public static WebElement button_UploadPlanogram(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='file'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='modal-footer']"));
		
		return element;
		
	}
	
	public static WebElement button_FirstStore(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//LABEL[@class='customCB'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@data-ng-show='isProcessed']"));
		
		return element;
		
	}

	
}
