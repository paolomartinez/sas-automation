package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cycleAddNICIObj {

	private static WebElement element = null;
	
	public static WebElement button_AddNewNICI(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Add New']"));
		
		return element;
		
	}
	
	public static WebElement button_UploadImage(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@type='file']"));
		
		return element;
		
	}
	
	public static WebElement textbox_ItemNumber(WebDriver driver) {
		
		element = driver.findElement(By.name("item_number"));
		
		return element;
		
	}
	
	public static WebElement textbox_Vendor(WebDriver driver) {
		
		element = driver.findElement(By.name("vendor"));
		
		return element;
		
	}
	
	public static WebElement select_Category(WebDriver driver) {
		
		element = driver.findElement(By.name("category"));
		
		return element;
		
	}
	
	public static WebElement textbox_UPC(WebDriver driver) {
		
		element = driver.findElement(By.name("upc"));
		
		return element;
		
	}
	
	public static WebElement textbox_ItemDesc(WebDriver driver) {
		
		element = driver.findElement(By.name("description"));
		
		return element;
		
	}
	
	public static WebElement button_FirstStore(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//LABEL[@class='customCB'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='modal-footer hideOnSubmit']"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@data-ng-show='isProcessed']"));
		
		return element;
		
	}
	
	public static WebElement alert_OK(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@class='confirm'][text()='OK']"));
		
		return element;
		
	}
}
