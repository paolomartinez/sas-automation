package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cycleAddPlanogramObj {

	private static WebElement element = null;
	
	public static WebElement button_Import(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Import excel']"));
		
		return element;
		
	}
	
	public static WebElement button_Export(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Export to xls']"));
		
		return element;
		
	}
	
	public static WebElement button_Browse(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@type='file']"));
		
		return element;
		
	}
	
	public static WebElement button_Upload(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement alert_Success(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='sweet-alert showSweetAlert visible']"));
		
		return element;
	}
	
}
