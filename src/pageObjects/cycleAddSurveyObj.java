package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cycleAddSurveyObj {
	
	private static WebElement element = null;
	
	// Survey
	public static WebElement button_AddNewSurvey(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Add New']"));
		
		return element;
		
	}
	
	public static WebElement textbox_SurveyTitle(WebDriver driver) {
		
		element = driver.findElement(By.name("surveyTitle"));
		
		return element;
		
	}
	
	public static WebElement button_StoreSelection(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SPAN[text()='store selection']"));
		
		return element;
		
	}
	
	public static WebElement button_SelectAllStores(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//LABEL)[8]"));
		
		return element;
		
	}
	
	public static WebElement button_SubmitStore(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-basic14'])"));
		
		return element;
		
	}
	
	public static WebElement button_Proceed(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-direction202']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Question(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='text'])[2]"));
		
		return element;
		
	}
	
	public static WebElement select_QuestionType(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='question.type']"));
		
		return element;
		
	}
	
	public static WebElement questionType_TextBox(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TEXTAREA[@rows='5']"));
		
		return element;
		
	}
	
	public static WebElement button_AddQuestion(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='submit'][text()='Add Question']"));
		
		return element;
		
	}
	
	public static WebElement button_SaveAsDraft(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@data-ng-disabled='isLoading'][text()='Save as Draft']"));
		
		return element;
		
	}
	
	public static WebElement button_PublishSurvey(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@data-ng-disabled='isLoading'][text()='publish new survey']"));
		
		return element;
		
	}
	
	public static WebElement button_OK(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@class='confirm'][text()='OK']"));
		
		return element;
		
	}
}
