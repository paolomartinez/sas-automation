package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cycleCreationObj {

	private static WebElement element = null;
	
	public static WebElement select_Customer(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SELECT[@data-ng-disabled='isLoading'])[1]"));
		
		return element;
	}
	
	public static WebElement select_Program(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SELECT[@data-ng-disabled='isLoading'])[2]"));
		
		return element;
		
	}
	
	public static WebElement select_Project(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//SELECT[@data-ng-disabled='isLoading'])[3]"));
		
		return element;
		
	}	
}
