package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;
import pageActions.manageCustomersAct;

public class cycleMgtObj {
	
	private static WebElement element = null;
	
	public static WebElement button_AddNewCycle(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='New Cycle']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Name(WebDriver driver) {
		
		element = driver.findElement(By.name("name"));
		
		return element;
		
	}
	
	public static WebElement textbox_StartDate(WebDriver driver) {
		
		element = driver.findElement(By.name("start_date"));
		
		return element;
		
	}
	
	public static WebElement textbox_EndDate(WebDriver driver) {
		
		element = driver.findElement(By.name("end_date"));
		
		return element;
		
	}
	
	public static WebElement button_AddInWindow(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement button_Docs(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='flaticon-documents11']"));
		
		return element;
	
	}
	
	public static WebElement button_CategoryReset(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TD[@data-ng-if='isCategoryReset && (isAll || isAdmin || isProgramManager || isProjectManager)']"));
		
		return element;
		
	}
	
	public static WebElement button_NICI(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TD[@data-ng-if='isNici && (isAll || isAdmin || isProgramManager || isProjectManager)']"));
		
		return element;
		
	}
	
	public static WebElement button_Survey(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TD[@data-ng-if='isSurvey && (isAll || isAdmin || isProgramManager || isProjectManager)']"));
		
		return element;
		
	}
	
	public static WebElement button_DistVoid(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TD[@data-ng-if='isDistributionVoid && (isAll || isAdmin || isProgramManager || isProjectManager)']"));
		
		return element;
		
	}
	
	public static WebElement button_PlanogramComp(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TD[@data-ng-if='isPlanogramCompliance && (isAll || isAdmin || isProgramManager || isProjectManager)']"));
		
		return element;
		
	}
	
	public static WebElement button_Schedule(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='flaticon-calendar-icons']"));
		
		return element;
		
	}
	
	public static WebElement button_Manage(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TD[@class='actionIcon ng-scope']"));
		
		return element;
		
	}
	
	public static WebElement button_CancelCycle(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Cancel Cycle"));
		
		return element;
		
	}
	
	public static WebElement button_Yes(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Reason(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='reason']"));
		
		return element;
		
	}
}
