package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class cycleTeamSchedObj {

	private static WebElement element = null;
	
	public static WebElement button_TeamScheduling(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='flaticon-people margin-r-xs']"));
		
		return element;
		
	}
	
	public static WebElement button_Schedule(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='flaticon-calendar-icons']"));
		
		return element;
		
	}
	
	public static WebElement button_AddNewSchedule(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@type='button'][text()='Add New']"));
		
		return element;
		
	}
	
	public static WebElement select_Team(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-model='teamScheduling.team.name']"));
		
		return element;
		
	}
	
	public static WebElement select_Store(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@data-ng-disabled='isLoading']"));
		
		return element;
		
	}
	
	public static WebElement textbox_ScheduleDate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='teamScheduling.scheduled_date']"));
		
		return element;
		
	}
	
	public static WebElement textbox_DueDate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='teamScheduling.due_by']"));
		
		return element;
		
	}
	
	public static WebElement textbox_ScheduledHours(WebDriver driver) {
		
		element = driver.findElement(By.name("team_name"));
		
		return element;
		
	}
	
	public static WebElement textbox_ShiftStart(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='start_time']"));
		
		return element;
		
	}
	
	public static WebElement textbox_ShiftEnd(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='end_time']"));
		
		return element;
		
	}
	
	public static WebElement button_SubmitSchedule(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='modal-footer hideOnSubmit']"));
		
		return element;
		
	}
	
	public static WebElement textbox_employeeID(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='manageShift.external_employee_id']"));
		
		return element;
	
	}
	
	public static WebElement textbox_employee(WebDriver driver) {
		
		element = driver.findElement(By.name("member_name"));
		
		return element;
		
	}
	
	public static WebElement textbox_DueBy(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='manageShift.due_by']"));
		
		return element;
		
	}
	
	public static WebElement toggle_TeamLead(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@type='checkbox']"));
		
		return element;
		
	}
	
	public static WebElement button_AddNewShift(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-plus79']"));
		
		return element;
		
	}
	
	public static WebElement button_SubmitShift(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='modal-footer hideOnSubmit ng-scope']"));
		
		return element;
		
	}
}
