package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class landingPageObj {
	private static WebElement element = null;
	
    // TOP PAGE OBJECTS
    public static WebElement button_Search(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-magnifying-glass32'])[1]"));
    	
    	return element;
    }
    
    public static WebElement button_SearchInput(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//INPUT[@type='text'])[1]"));
    	
    	return element;
    }
    
    public static WebElement button_LanguageMenu(WebDriver driver){
    	
    	element = driver.findElement((By.id("languageMenu")));
    	
    	return element;
    }
    
    public static WebElement button_Notifications(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//I[@class='Flaticonflaticon-bell46']"));
    	
    	return element;
    }
    
    public static WebElement button_SettingsArrow(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//I[@class='fIconflaticon-arrow486']"));
    	
    	return element;
    }
    
    public static WebElement button_Photo(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//IMG[@class='img-responseive'])[1]"));
    	
    	return element;
    }
    
    // MIDDLE PAGE OBJECTS
	
	public static WebElement select_Customer(WebDriver driver){
		
		element = driver.findElement(By.name("dashboardReportingCustomer"));
		
		return element;
	}


	public static WebElement date_From(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='dashboardReporting.scheduled_dt_from']"));
		
		return element;
	}
	
	public static WebElement date_To(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@data-ng-model='dashboardReporting.scheduled_dt_to']"));
		
		return element;
	}
	
    public static WebElement button_Filter(WebDriver driver){

     	element = driver.findElement(By.xpath("//BUTTON[@type='submit'][text()='Filter']"));
     	
     	return element;
     }
    
    public static WebElement link_Reporting(WebDriver driver){
    	
    	element = driver.findElement(By.partialLinkText("Reporting"));
    	
    	return element;
    }
    
}
