package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class leftMenuObj {
	private static WebElement element = null;
	
    public static WebElement button_Customers(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[@class='flaticon-silhouette121'][text()='Customers']"));
    	
    	return element;
    }
    
    public static WebElement button_ManageCustomers(WebDriver driver){
    	
    	element = driver.findElement(By.linkText("Manage Customers"));
    	
    	return element;
    }
    
    public static WebElement button_AddCustomer(WebDriver driver){
    	
    	element = driver.findElement(By.linkText("Add New Customer"));
    	
    	return element;
    }
    
    
    public static WebElement button_Program(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[@class='flaticon-notepad8'][text()='Program']"));
    	
    	return element;
    }
    
    public static WebElement button_ManagePrograms(WebDriver driver){
    	
    	element = driver.findElement(By.linkText("Manage Programs"));
    	
    	return element;
    }
    
    public static WebElement button_AddProgram(WebDriver driver){
    	
    	element = driver.findElement(By.linkText("Add New Program"));
    	
    	return element;
    }
    
    
    public static WebElement button_Projects(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[@class='flaticon-folder232'][text()='Projects']"));
    	
    	return element;
    }
    
    public static WebElement button_ManageProjects(WebDriver driver){
    	
    	element = driver.findElement(By.linkText("Manage Projects"));
    	
    	return element;
    }
    
    public static WebElement button_AddProject(WebDriver driver){
    	
    	element = driver.findElement(By.linkText("Add New Project"));
    	
    	return element;
    }
    
    
    public static WebElement button_Cycles(WebDriver driver){
    	
    	element = driver.findElement(By.className("flaticon-notepad8"));
    			
		return element;
    }
    
    public static WebElement button_Alerts(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//I[@class='fa fa-bell']"));
    	
    	return element;
    }
    
    public static WebElement button_Workforce(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[@class='flaticon-people'][text()='Workforce']"));
    	
    	return element;
    }
    
    public static WebElement button_Operations(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//I[@class='fa fa-building']"));
    	
    	return element;
    }
    
    public static WebElement button_Schedule(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Schedule ']"));
    	
    	return element;
    }
    
    public static WebElement button_FieldDataManagement(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Field Data Management']"));
    	
    	return element;
    }
    
    public static WebElement button_TimeExpense(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Time Expense']"));
    	
    	return element;
    }
    
    public static WebElement button_BillPrep(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Bill Prep']"));
    	
    	return element;
    }
    
    
    public static WebElement button_OperationsReport(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[@class='flaticon-silhouette121'][text()='Operations Report']"));
    	
    	return element;
    }
    
    public static WebElement button_PayrollReport(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[@class='flaticon-homework'][text()='Payroll Report']"));
    	
    	return element;
    }
    
    public static WebElement button_Aggregate(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Aggregate (ADP) ']"));
    	
    	return element;
    }
    
    public static WebElement button_Shift(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Shift']"));
    	
    	return element;
    }
    
    public static WebElement button_CostCenter(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Cost Center']"));
    	
    	return element;
    }
    
    public static WebElement button_Expense(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//SPAN[text()='Expense']"));
    	
    	return element;
    }
}
