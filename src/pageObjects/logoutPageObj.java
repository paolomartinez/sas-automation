package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class logoutPageObj {
	
	private static WebElement element = null;
	
    public static WebElement button_SettingsArrow(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//A[@id='userPref']"));
    	
    	return element;
    }
    
    public static WebElement button_logOut(WebDriver driver){
    	
    	element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-arrow5']"));
    	
    	return element;
    }
    
    public static WebElement textbox_UserName(WebDriver driver){

    	element = driver.findElement(By.id("login-email"));

    	return element;

     }

    public static WebElement textbox_Password(WebDriver driver){

     	element = driver.findElement(By.id("login-password"));

     	return element;

     }

    public static WebElement button_LogIn(WebDriver driver){

     	element = driver.findElement(By.xpath("//BUTTON[@type='submit'][text()='Login']"));
     	
     	return element;
     }
}
