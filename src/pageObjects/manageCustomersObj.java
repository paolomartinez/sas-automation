package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class manageCustomersObj {
	private static WebElement element = null;
	
	public static WebElement table_CustomerTable(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageTabContent ng-scope']"));
		
		return element;
	}
	
	public static WebElement button_ActiveCustomers(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Active Customers"));

		return element;
	}
	
	public static WebElement button_DraftCustomers(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Draft Customers"));
		
		return element;
	}
	
	public static WebElement button_ArchivedCustomers(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Archived Customers"));
		
		return element;
	}
	
	public static WebElement button_AddCustomer(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//A[@type='button'][text()='Add New Customer']"));
		
		return element;
	}
	
	public static WebElement button_Archive(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Archive"));
		
		return element;
		
	}
	
	public static WebElement button_Yes(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Reason(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TEXTAREA[@data-ng-model='reason']"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement button_Manage(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//TD[@class='actionIcon'])[1]"));
		
		return element;
		
	}
	
	
}
