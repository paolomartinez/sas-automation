package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class manageProgramsObj {

	private static WebElement element = null;
	
	public static WebElement table_ProgramTable(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageTabContent activePrograms ng-scope ng-isolate-scope']"));
		
		return element;
	}
	
	public static WebElement button_ActivePrograms(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Active Programs"));

		return element;
	}
	
	public static WebElement button_ArchivedPrograms(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Archived Programs"));
		
		return element;
	}
	
	public static WebElement select_Customer(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//SELECT[@st-input-event='change']"));
		
		return element;
	}
	
	//NOTE: Add New Program button not available until a Customer is selected
	
	public static WebElement textbox_SearchPrograms(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='text'])[3]"));
		
		return element;
		
	}
	
	public static WebElement button_ManageMostRecentProg(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-tool'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_Archive(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Archive"));
		
		return element;
		
	}
	
	public static WebElement button_Yes(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement textbox_Reason(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TEXTAREA[@data-ng-model='reason']"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
}
