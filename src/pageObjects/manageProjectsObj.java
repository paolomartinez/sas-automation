package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class manageProjectsObj {

	private static WebElement element = null;
	
	public static WebElement table_ProjectTable(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//DIV[@class='pageTabContent ng-scope']"));
		
		return element;
		
	}
	
	public static WebElement button_ActiveProjects(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Active Projects"));

		return element;
		
	}
	
	public static WebElement button_DraftProjects(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Draft Projects"));
		
		return element;
		
	}
	
	public static WebElement button_ArchivedProjects(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Archived Projects"));
		
		return element;
		
	}
	
	public static WebElement button_AddProject(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//A[@type='button'][text()='Add New Project']"));
		
		return element;
		
	}
	
	public static WebElement sort_UnsortedStartDate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TH[@class='sort'][text()='Start Date']"));
		
		return element;
		
	}
	
	public static WebElement sort_AscendingStartDate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TH[@class='sort st-sort-ascent'][text()='Start Date']"));
		
		return element;
		
	}
	
	public static WebElement sort_DescendingStartDate(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TH[@class='sort st-sort-descent'][text()='Start Date']"));
		
		return element;
		
	}
	
	public static WebElement button_ManageMostRecentProj(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-tool'])[2]"));
		
		return element;
		
	}
	
	public static WebElement button_ManageSecondProj(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//I[@class='fIcon flaticon-tool'])[3]"));
		
		return element;
		
	}
	
	public static WebElement button_ApproveReject(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Approve/Reject"));
		
		return element;
		
	}
	
	public static WebElement button_Status(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TH[@class='sort text-center'][text()='Status']"));
		
		return element;
		
	}
	
	public static WebElement button_DescendingStatus(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TH[@class='sort text-center st-sort-ascent'][text()='Status']"));
		
		return element;
		
	}
	
	public static WebElement textbox_SearchProjects(WebDriver driver) {
		
		element = driver.findElement(By.xpath("(//INPUT[@type='text'])[3]"));
		
		return element;
		
	}
	
	public static WebElement button_Manage(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//BUTTON[@id='activeRow1']"));
		
		return element;
		
	}
	
	public static WebElement button_PauseProject(WebDriver driver) {
		
		element = driver.findElement(By.linkText("Pause Project"));
		
		return element;
		
	}
	
	public static WebElement textbox_Reason(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//TEXTAREA[@class='reasonTextarea hideOnSubmit ng-pristine ng-untouched ng-invalid ng-invalid-required']"));
		
		return element;
		
	}
	
	public static WebElement textbox_DateEffective(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//INPUT[@ng-model='pause_effective_date']"));
		
		return element;
		
	}
	
	public static WebElement button_Submit(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='fIcon flaticon-basic14']"));
		
		return element;
		
	}
	
	public static WebElement icon_Paused(WebDriver driver) {
		
		element = driver.findElement(By.xpath("//I[@class='hasStatus statusPaused flaticon-record13']"));
		
		return element;
		
	}
	
}
