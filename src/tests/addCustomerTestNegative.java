package tests;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import common.dataHandling;
import common.testConfig;
import pageActions.addCustomerStep1Act;
import pageActions.addCustomerStep2Act;
import pageActions.addCustomerStep3Act;
import pageActions.addCustomerStep4Act;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;

public class addCustomerTestNegative {

private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "addCustomerNegativeSheet");
		
		//Set driver
		testConfig.setDriver();
		
		int j = 4;
		
		for(int i = 2; i <= dataHandling.getNumRows("addCustomerNegativeSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			
			//Step 1 Variables
			//add time stamp to make each customer unique since duplicate customer names not allowed	
			String customerName = dataHandling.getCellData(i, 3) + timeStamp;

			dataHandling.setCellData(customerName, i, j);	
			
			String customerType = dataHandling.getCellData(i, j+1);
			String parentCompany = dataHandling.getCellData(i, j+2);
			String address = dataHandling.getCellData(i, j+3);
			String country = dataHandling.getCellData(i, j+4);
			String state = dataHandling.getCellData(i, j+5);
			String city = dataHandling.getCellData(i, j+6);
			String zip = dataHandling.getCellData(i, j+7);
			String contactName = dataHandling.getCellData(i, j+8);
			String primaryPhone = dataHandling.getCellData(i, j+9);
			String email = dataHandling.getCellData(i, j+10);
			String paymentTerms = dataHandling.getCellData(i, j+11);
			String creditAmt = dataHandling.getCellData(i, j+12);
			String preBill = dataHandling.getCellData(i, j+13);
			String title = dataHandling.getCellData(i, j+14);
			String department = dataHandling.getCellData(i, j+15);
			
			//Step 2 Variables
			String storeName = dataHandling.getCellData(i, j+16);
			String storeNum = dataHandling.getCellData(i, j+17);
			String storeAddress = dataHandling.getCellData(i, j+18);
			String storeCountry = dataHandling.getCellData(i, j+19);
			String storeState = dataHandling.getCellData(i, j+20);
			String storeCity = dataHandling.getCellData(i, j+21);
			String storeZip = dataHandling.getCellData(i, j+22);
			String storePhone = dataHandling.getCellData(i, j+23);
			String storeEmail = dataHandling.getCellData(i, j+24);
			String categoryNum = dataHandling.getCellData(i, j+25);
			String categoryName = dataHandling.getCellData(i, j+26);
			String photoReq = dataHandling.getCellData(i, j+27);
			
			//Step 3 Variables
			String goodUserName = (customerName.substring(0, customerName.length()/2)) + timeStamp;
			String goodAddress = (address.substring(0, address.length()/2)) + timeStamp;
			String goodEmail = email.substring(email.length()/2, email.length());
			
			//NOTE so in step 1 you can put out of bounds max values and it only cares when you try to submit
			//		in step 3
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
				
				//put something about expected result here??
				
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//execute Step 1 of Add New Customer
				addCustomerStep1Act.Execute(driver, customerName, customerType, parentCompany,
										address, country, state, city, zip,
										contactName, primaryPhone, email, paymentTerms,
										creditAmt, preBill, title, department, goodUserName, goodAddress, goodEmail);
				
				//execute Step 2 of Add New Customer
				addCustomerStep2Act.Execute(driver, storeName, storeNum, storeAddress, storeCountry, 
						storeState, storeCity, storeZip, storePhone, storeEmail, categoryNum, categoryName, photoReq);
				
				//execute Step 3 of Add New Customer				
				addCustomerStep3Act.Execute(driver, customerName, customerType,
						address, country, state, city, zip,
						contactName, primaryPhone, email, paymentTerms,
						creditAmt, preBill, title, department,
						storeName, storeNum, storeAddress,  
						storeState, storeCity, storeZip, storePhone, 
						storeEmail, categoryNum, categoryName, photoReq, 
						goodUserName, goodAddress, goodEmail);

				//execute Step 4 of Add New Customer 
				addCustomerStep4Act.Execute(driver, customerName);
				System.out.println("add customer negative test complete");
				
				dataHandling.setCellData("PASS", i, j+29);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, j+29);
				
			}
		}
	}
}
