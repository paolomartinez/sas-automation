package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.cycleAddCategoryAct;
import pageActions.cycleAddDistAct;
import pageActions.cycleAddNICIAct;
import pageActions.cycleAddPlanogramAct;
import pageActions.cycleAddSurveyAct;
import pageActions.cycleCreationAct;
import pageActions.cycleMgtAct;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;

public class addCycleTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "cyclesSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("cyclesSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = dateFormat.format(date);
			System.out.println(sDate);
			
			int j = 3;
			
			//Cycle Creation variables
			String customer = dataHandling.getCellData(i, j);
			String program = dataHandling.getCellData(i, j+1);
			String project = dataHandling.getCellData(i, j+2);
			
			//Survey Creation variables
			String store = dataHandling.getCellData(i, j+3);
			String title = dataHandling.getCellData(i, j+4);
			String question = dataHandling.getCellData(i, j+5);
			String type = dataHandling.getCellData(i, j+6);
			
			//Cycle Management and Team Creation variables
			String cycleName = dataHandling.getCellData(i, j+7) + timeStamp;
			dataHandling.setCellData(cycleName, i, j+8);
			String teamName = dataHandling.getCellData(i, j+9);
			String storeSelect = dataHandling.getCellData(i, j+10);
			String hours = dataHandling.getCellData(i, j+11);
			String id = dataHandling.getCellData(i, j+12);
			String employee = dataHandling.getCellData(i, j+13);
			String isTeamLead = dataHandling.getCellData(i, j+14);
			String shiftStart = dataHandling.getCellData(i, j+15);
			String shiftEnd = dataHandling.getCellData(i, j+16);
			
			//NICI Creation variables
			String itemNum = dataHandling.getCellData(i, j+17);
			String vendor = dataHandling.getCellData(i, j+18);
			String category = dataHandling.getCellData(i, j+19);
			String upc = dataHandling.getCellData(i, j+20);
			String description = dataHandling.getCellData(i, j+21);
			
			//Category Reset variables
			String cr_category = dataHandling.getCellData(i, j+22);
			String cr_id = dataHandling.getCellData(i, j+23);
			String cr_size = dataHandling.getCellData(i, j+24);
			String cr_sizeType = dataHandling.getCellData(i, j+25);
			String cr_hours = dataHandling.getCellData(i, j+26);
			String cr_minutes = dataHandling.getCellData(i, j+27);
			
			//Planogram Compliance variables
			String planogramFile = dataHandling.getCellData(i, j+28);
			
			System.out.println("isTeamL: " + isTeamLead);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
			
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//go to cycle management page
				String url = testConfig.URL + "sasretail/activation/cycle-management/";
				driver.navigate().to(url);
				//driver.navigate().to("https://test01.sasretail.com/en/sasretail/activation/cycle-management/");
				Thread.sleep(5000);
				
				//execute first step of cycle creation
				cycleCreationAct.Execute(driver, customer, program, project, cycleName, sDate, sDate);
				
				String currUrl = driver.getCurrentUrl();
				System.out.println("current url is: " + currUrl);
				
				//create a new item cut in for the cycle
				cycleAddNICIAct.Execute(driver, itemNum, vendor, category, upc, description, url);
	
				//create a category reset for the cycle
				cycleAddCategoryAct.Execute(driver, cr_category, cr_id, cr_size, cr_sizeType, cr_hours, cr_minutes, url);
				
				//create a distribution void for the cycle
				cycleAddDistAct.Execute(driver, itemNum, vendor, category, upc, description, url);
				
				//create a survey for the cycle
				cycleAddSurveyAct.Execute(driver, store, title, question, type, url);
				
				//assign employees to the cycle
				cycleMgtAct.Execute(driver, sDate, sDate, teamName, storeSelect, hours, shiftStart, shiftEnd, id, employee, isTeamLead);
					
				//create a planogram compliance for the cycle
//				cycleAddPlanogramAct.Execute(driver);
			
				System.out.println("add cycle test complete");
				//dataHandling.setCellData("PASS", i, 33);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				//dataHandling.setCellData("FAIL", i, 33);
				
			}
		}
	}
}

