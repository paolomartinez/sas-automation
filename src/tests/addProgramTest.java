package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.addProgramStep1Act;
import pageActions.addProgramStep2Act;
import pageActions.addProgramStep3Act;
import pageActions.addProgramStep4Act;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;


public class addProgramTest {
	
	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "addProgramSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("addProgramSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = dateFormat.format(date);
			System.out.println(sDate);
			
			//Step 1 variables
			String customerName = dataHandling.getCellData(i, 3);
			String programName = dataHandling.getCellData(i, 4) + timeStamp;
			String teamBased = dataHandling.getCellData(i, 6);
			String numTeams = dataHandling.getCellData(i, 7);
			String avgSize = dataHandling.getCellData(i, 8);
			
			//Step 2 Variables
			String zip = dataHandling.getCellData(i, 9);
			
			//Step 3 Variables
			String lead = dataHandling.getCellData(i, 10);
			String fieldSupervisor = dataHandling.getCellData(i, 11);
			String merchandiser = dataHandling.getCellData(i, 12);
			
			System.out.println("lead is: " + lead);
			System.out.println("field supervisor is: " + fieldSupervisor);
			System.out.println("merchandiser is: " + merchandiser);
			
			dataHandling.setCellData(customerName, i, 3);
			dataHandling.setCellData(programName, i, 5);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
			
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//execute add program step 1 action
				addProgramStep1Act.Execute(driver, customerName, programName, sDate, sDate, 
											teamBased, numTeams, avgSize);
				
				//execute add program step 2 action
				addProgramStep2Act.Execute(driver, zip);
				
				//execute add program step 3 action
				addProgramStep3Act.Execute(driver, lead, fieldSupervisor, merchandiser);
				
				//execute add program step 4 action
				addProgramStep4Act.Execute(driver);
				
				System.out.println("add new program test complete");
				dataHandling.setCellData("PASS", i, 13);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 13);
				
			}
		}
	}
}
