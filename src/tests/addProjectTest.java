package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.addProjectStep1Act;
import pageActions.addProjectStep2Act;
import pageActions.addProjectStep3Act;
import pageActions.addProjectStep4Act;
import pageActions.approveProjectAct;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;

public class addProjectTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "addProjectSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("addProjectSheet"); i++) {
			
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = dateFormat.format(date);
			System.out.println(sDate);
			
			//Step 1 variables
			String projectName = dataHandling.getCellData(i, 3) + timeStamp;
			dataHandling.setCellData(projectName, i, 4);
			String projectTimestamp = dataHandling.getCellData(i, 4);
			String customer = dataHandling.getCellData(i, 5);
			String program = dataHandling.getCellData(i, 6);
			String projectType = dataHandling.getCellData(i, 7);
			String projectManager = dataHandling.getCellData(i, 8);
			String billPrepLead = dataHandling.getCellData(i, 9);
			
			//Step 2 Variables
			String serviceType = dataHandling.getCellData(i, 10).toLowerCase();
			String occurrence = dataHandling.getCellData(i, 11);
			String activeDays = dataHandling.getCellData(i, 12);
			
			//Step 3 Variables
			String client = dataHandling.getCellData(i, 14);
			String billingContact = dataHandling.getCellData(i, 15);
			String loaContact = dataHandling.getCellData(i, 16);
			String rateType = dataHandling.getCellData(i, 17);
			String priceType = dataHandling.getCellData(i, 18);
			String billRate = dataHandling.getCellData(i, 19);
			String ratePerVisit = dataHandling.getCellData(i, 20);
			String billingTerms = dataHandling.getCellData(i, 21);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
			
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//execute add project step 1 action
				addProjectStep1Act.Execute(driver, projectName, customer, program, projectType, 
											sDate, sDate, projectManager, billPrepLead);
				
				//execute add project step 2 action
				addProjectStep2Act.Execute(driver, serviceType, occurrence, activeDays);

				//execute add project step 3 action
				addProjectStep3Act.Execute(driver, client, billingContact, loaContact, rateType, 
											priceType, billRate, ratePerVisit, billingTerms);
				
				//execute add project step 4 action
				addProjectStep4Act.Execute(driver);
				
				//approve it twice
				/* STEPS
				 * Go to manage projects
				 * Tap Start Date twice
				 * Tap on the manage icon
				 * Tap Approve/Reject
				 * Approve
				 * Submit
				 * Go back to Draft Projects
				 * Repeat one more time
				 */
				approveProjectAct.Execute(driver); //fin approval
				approveProjectAct.secondApproval(driver); //ops approval
	
				approveProjectAct.verifyActiveStatus(driver, projectTimestamp);
				
				System.out.println("add new project test complete");
				dataHandling.setCellData("PASS LOOK", i, 23);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 23);
				
			}
		}
	}
}
