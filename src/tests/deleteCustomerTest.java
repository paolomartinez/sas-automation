package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import common.dataHandling;
import common.testConfig;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;
import pageActions.manageCustomersAct;

public class deleteCustomerTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "addCustomerPositiveSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("addCustomerPositiveSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String customer = dataHandling.getCellData(i, 4);
			System.out.println("customer: " + customer);
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
				
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//execute archive customer action
				//TODO currently this just deletes the first customer at the top of the page
				manageCustomersAct.deleteCustomer(driver, customer);
				
				System.out.println("manage customers test complete");
				
				dataHandling.setCellData("PASS", i, 35);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 35);
				
			}
		}
		
		// DELETES THE NEGATIVE TEST CASE CUSTOMERS
		for(int i = 1; i <= dataHandling.getNumRows("addCustomerNegativeSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String customer = dataHandling.getCellData(i, 4);
			System.out.println("customer: " + customer);
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
				
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//execute archive customer action
				//TODO currently this just deletes the first customer at the top of the page
				manageCustomersAct.deleteCustomer(driver, customer);
				
				System.out.println("manage customers test complete");
				
				dataHandling.setCellData("PASS", i, 35);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 35);
				
			}
		}
	}
}