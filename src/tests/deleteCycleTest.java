package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import common.dataHandling;
import common.testConfig;
import pageActions.cycleCreationAct;
import pageActions.cycleMgtAct;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;
import pageActions.manageProgramsAct;

public class deleteCycleTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {

		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "cyclesSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("cyclesSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			
			int j = 3;
			
			String timeStamp = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
			
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = dateFormat.format(date);
			System.out.println(sDate);
			
			//Cycle Creation variables
			String customer = dataHandling.getCellData(i, j);
			String program = dataHandling.getCellData(i, j+1);
			String project = dataHandling.getCellData(i, j+2);
			String cycleName = dataHandling.getCellData(i, j+7) + timeStamp;
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
			
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				String url = testConfig.URL + "sasretail/activation/cycle-management/";
				driver.navigate().to(url);
				//driver.navigate().to("https://test01.sasretail.com/en/sasretail/activation/cycle-management/");
				Thread.sleep(5000);
				
				//execute first step of cycle creation
				cycleCreationAct.Execute(driver, customer, program, project, cycleName, sDate, sDate);
				
				String currUrl = driver.getCurrentUrl();
				System.out.println("current uurl is: " + currUrl);
				
				//execute delete cycle action
				cycleMgtAct.deleteCycle(driver);
				
				System.out.println("delete cycle test complete");
				//dataHandling.setCellData("PASS", i, 23);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				//dataHandling.setCellData("FAIL", i, 23);
				
			}
		}
	}
}

