package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import common.dataHandling;
import common.testConfig;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;
import pageActions.manageProgramsAct;

public class deleteProgramTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {

		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "addProgramSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("addProgramSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			
			String program = dataHandling.getCellData(i, 5);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
			
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				manageProgramsAct.deleteProgram(driver, program);
				System.out.println("delete program test complete");
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 13);
				
			}
		}
	}
}
