package tests;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;
import pageActions.manageProjectsAct;

public class deleteProjectTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {

		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "addProjectSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("addProjectSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			
			String project = dataHandling.getCellData(i, 4);
			Date date = new Date();
			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String sDate = dateFormat.format(date);
			System.out.println(sDate);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
			
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				manageProjectsAct.deleteProject(driver, project, sDate);
				System.out.println("delete project test complete");
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 13);
				
			}
		}
	}
}