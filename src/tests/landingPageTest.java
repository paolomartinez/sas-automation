package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.landingPageAct;
import pageActions.loginPageAct;

public class landingPageTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "landingSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("landingSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String expectedRes = dataHandling.getCellData(i, 3);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute landing page action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
				
				//if expected PASS, do landing page verification
				//else just fail?
				if(expectedRes.equals("PASS")) {
					
					landingPageAct.Execute(driver);
					System.out.println("landing page test complete");
					dataHandling.setCellData("PASS", i, 4);
					
				} else {
					
					dataHandling.setCellData("FAIL", i, 4);
					
				}
				
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 4);
				
			}
		}
	}
}
