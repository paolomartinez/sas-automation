package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import common.dataHandling;
import common.testConfig;
import pageActions.leftMenuAct;
import pageActions.loginPageAct;
import pageActions.manageCustomersAct;

public class manageCustomersTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "leftMenuSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("leftMenuSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute left menu action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
				
				//put something about expected result here??
				
				leftMenuAct.Execute(driver);
				System.out.println("left menu test complete");
				
				//execute manage customers action
				manageCustomersAct.Execute(driver);
								
				System.out.println("manage customers test complete");
				
				dataHandling.setCellData("PASS", i, 4);
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 4);
				
			}
		}
	}
}