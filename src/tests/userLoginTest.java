package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.landingPageAct;
import pageActions.loginPageAct;

public class userLoginTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {
						
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "loginSheet");
		
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each row is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("loginSheet"); i++) {
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String sPassword = dataHandling.getCellData(i, 2);
			String expectedRes = dataHandling.getCellData(i, 3);
			
			//no empty strings for user and password
			if(sUserName != "" && sPassword != "") {
				
				//if expected PASS, do landing page verification
				//else do login page verification
				if(expectedRes.equals("PASS")) {
					
					loginPageAct.Execute(driver, sUserName, sPassword);
					landingPageAct.Execute(driver);
					dataHandling.setCellData("PASS", i, 4);
					
				} else {
					
					loginPageAct.Execute(driver, sUserName, sPassword);
					loginPageAct.verifyErrorMessage(driver);
					loginPageAct.verifyLoginPage(driver);
					dataHandling.setCellData("FAIL", i, 4);
					
				}
				
				System.out.println("execution " + i + " complete");
				driver.quit();
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 4);
			}
		}
	}
}