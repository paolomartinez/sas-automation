package tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import common.dataHandling;
import common.testConfig;
import pageActions.landingPageAct;
import pageActions.loginPageAct;
import pageActions.logoutPageAct;

public class userVerificationTest {

	private static WebDriver driver = null;
	
	public static void main(String[] args) throws Exception {

		/* TEST CASES
		 * Positive: all login page controls exist, login correct, landing page loads
		 * Negative: login page controls don't exist, login incorrect, so landing page doesn't load
		 */
		
		//negative test: after logging in, go back to dashboard url
		//				the controls for logged in shouldn't be present
		
		//start a test, try to go without logging in at all
		
		//Open the Excel file for specified sheet
		dataHandling.setExcelFile(testConfig.Path_TestData + testConfig.File_TestData, "verificationSheet");
				
		//Set driver
		testConfig.setDriver();
		
		//Parse through Excel sheet
		//Each cell is a new test case
		for(int i = 1; i <= dataHandling.getNumRows("verificationSheet"); i++) {
			
			driver = new ChromeDriver();
			testConfig.instantiateDriver(driver);
			
			String sUserName = dataHandling.getCellData(i, 1);
			String user = sUserName.split("\\@")[0].toUpperCase();
			String sPassword = dataHandling.getCellData(i, 2);
			
			if(sUserName != "" && sPassword != "") {
				
				//Execute login page action, then execute landing page action
				loginPageAct.Execute(driver, sUserName, sPassword);
				System.out.println("login test complete");
				
				landingPageAct.Execute(driver);
				System.out.println("landing page test complete");
				
				logoutPageAct.Execute(driver, user, i);
				System.out.println("user verification test complete");
				System.out.println("execution " + i + " complete");
				driver.quit();
				
				//Send the PASS value to the Excel sheet in result column
				dataHandling.setCellData("PASS", i, 4);
				
			} else {
				
				System.out.println("User/password is null");
				driver.quit();
				dataHandling.setCellData("FAIL", i, 4);
				
			}
			
	   }
	   
	}
}
